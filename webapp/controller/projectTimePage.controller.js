sap.ui.define([
	'sap/m/Button',
	'sap/m/Dialog',
	'sap/m/Label',
	'sap/m/Text',
	'sap/m/TextArea',
	'sap/m/MessageBox',
	'sap/ui/core/Fragment',
	"jquery.sap.global", "sap/ui/model/json/JSONModel",
	'sap/ui/unified/CalendarLegendItem',
	'sap/ui/unified/DateTypeRange',
	'sap/ui/unified/DateRange',
	'com/deloitte/timesheet/model/Constants',
	'com/deloitte/timesheet/model/Common',
	"com/deloitte/timesheet/controller/BaseController",
	'sap/m/MessageToast'
], function (Button, Dialog, Label, Text, TextArea, MessageBox, Fragment, jQuery, JSONModel, CalendarLegendItem, DateTypeRange, DateRange,
	Constants, Common, BaseController, MessageToast) {
	"use strict";
	var projectCode = "";
	var activityNo = "";
	var worktypeCode = "";
	var projectName = "";
	var customerName = "";
	var worktypeName = "";

	var sselectedWeek = null;
	var weekDates = null; //seçili hafta
	var disabledDates = null; //izin-tatiller
	/*seçili haftaya girilmiş gün-saat*/
	var monTotalHour = null;
	var tueTotalHour = null;
	var wedTotalHour = null;
	var thurTotalHour = null;
	var friTotalHour = null;
	var satTotalHour = null;
	/*seçili projeye girilmiş gün-saat*/
	var projectMonHour = null;
	var projectTueHour = null;
	var projectWedHour = null;
	var projectThurHour = null;
	var projectFriHour = null;
	var projectSatHour = null;
	/* servisden gelen proje-saat sonucunun indexini gösterir
	 */
	var prMonCount = null;
	var prTueCount = null;
	var prWedCount = null;
	var prThurCount = null;
	var prFriCount = null;
	var prSatCount = null;

	/* servisden gelen status günlere göre
	 */
	var monStatus = null;
	var tueStatus = null;
	var wedStatus = null;
	var thurStatus = null;
	var friStatus = null;
	var satStatus = null;

	var itemsTotalHour = null;
	var itemsProjectHour = null;

	var focusedDay = "";
	var mySelf = null;
	return BaseController.extend("com.deloitte.timesheet.controller.projectTimePage", {

		onInit: function () {
			//this._oRouter = this.getRouter();
			this.getRouter().attachRouteMatched(this.handleRouteMatched, this);
		},
		handleRouteMatched: function (evt) {
			//Check whether is the detail page is matched.
			if (evt.getParameter("name") !== "projectTimePage") {
				return;
			}

			if (this.controlCancel("timeSheetSummary") === true) {
				sap.ui.getCore().byId(this.createId("btnCancel")).setVisible(false);
			} else {
				sap.ui.getCore().byId(this.createId("btnCancel")).setVisible(true);
			}
			this.showBusyIndicator(3000, 0);

			mySelf = this;

			projectCode = "";
			activityNo = "";
			worktypeCode = "";
			projectName = "";
			customerName = "";
			worktypeName = "";

			sselectedWeek = null;
			weekDates = null; //seçili hafta
			disabledDates = null; //izin-tatiller
			/*seçili haftaya girilmiş gün-saat*/
			monTotalHour = null;
			tueTotalHour = null;
			wedTotalHour = null;
			thurTotalHour = null;
			friTotalHour = null;
			satTotalHour = null;
			/*seçili projeye girilmiş gün-saat*/
			projectMonHour = null;
			projectTueHour = null;
			projectWedHour = null;
			projectThurHour = null;
			projectFriHour = null;
			projectSatHour = null;
			/* servisden gelen proje-saat sonucunun indexini gösterir
			 */
			prMonCount = null;
			prTueCount = null;
			prWedCount = null;
			prThurCount = null;
			prFriCount = null;
			prSatCount = null;

			/* servisden gelen status günlere göre
			 */
			monStatus = null;
			tueStatus = null;
			wedStatus = null;
			thurStatus = null;
			friStatus = null;
			satStatus = null;

			itemsTotalHour = null;
			itemsProjectHour = null;

			this.setFocusIssues("monInput");
			this.setFocusIssues("tueInput");
			this.setFocusIssues("wedInput");
			this.setFocusIssues("thurInput");
			this.setFocusIssues("friInput");
			this.setFocusIssues("satInput");

			focusedDay = "";

			projectCode = Common.getDataFromModel(Constants.TIMESHEET_ENTRY_PROJECT_CODE);
			activityNo = Common.getDataFromModel(Constants.TIMESHEET_ENTRY_ACTIVITY_NO);
			if (activityNo.includes("(") !== true && activityNo.includes(")") !== true) {
				activityNo = "(" + activityNo + ")";
			}
			worktypeCode = Common.getDataFromModel(Constants.TIMESHEET_ENTRY_WORK_TYPE_CODE);
			projectName = Common.getDataFromModel(Constants.TIMESHEET_ENTRY_PROJECT_NAME);
			customerName = Common.getDataFromModel(Constants.TIMESHEET_ENTRY_CUSTOMER_NAME);
			worktypeName = Common.getDataFromModel(Constants.TIMESHEET_ENTRY_WORK_TYPE_NAME);
			sselectedWeek = Common.getDataFromModel(Constants.TIMESHEET_ENTRY_PERIOD);
			weekDates = sselectedWeek.selectedDate; //seçili hafta
			disabledDates = sselectedWeek.disabledDates; //izin-tatiller

			var allData = new sap.ui.model.json.JSONModel();
			var json = {};

			var maxDate = weekDates[5]; //seçilen haftanın cumartesi günü
			var minDate = weekDates[0]; //seçilen haftanın pazartesi günü
			var oPage = sap.ui.getCore().byId(this.createId("pageTitle"));
			oPage.setTitle(Common.OnlyDateShow(minDate) + " - " + Common.OnlyDateShow(maxDate));

			this.showLabels(); //label gosterim

			var urlProjectHour = "TimeSheetSet(Pernr='" + Common.PersonnelNo() + "',Datefrom=" + Common.DateToString(minDate, true) +
				",Dateto=" + Common.DateToString(
					maxDate, true) + ")/ActivitySet";
			/*todo:urlProjectHour2 test için kullanılan adrestir kullanılması gereken url dir.
			 */
			//	var urlProjectHour2="TimeSheetSet(Pernr='90000105',Datefrom=datetime'2016-10-01T00%3A00%3A00',Dateto=datetime'2017-01-30T00%3A00%3A00')/ActivitySet";		

			var oODataJSONModelProjectHour = Common.CallSAPMethod(urlProjectHour);
			itemsProjectHour = oODataJSONModelProjectHour.getData().results;

			var urlTotalHour = "TimeSheetSet(Pernr='" + Common.PersonnelNo() + "',Datefrom=" + Common.DateToString(minDate, true) + ",Dateto=" +
				Common.DateToString(
					maxDate, true) + ")/TimeSheetDateSet";
			/*todo:urlTotalHour2 test için kullanılan adrestir kullanılması gereken url dir.
			 */
			//	var urlTotalHour2="TimeSheetSet(Pernr='90000105',Datefrom=datetime'2016-10-01T00%3A00%3A00',Dateto=datetime'2017-01-30T00%3A00%3A00')/TimeSheetDateSet";		

			var oODataJSONModelTotalHour = Common.CallSAPMethod(urlTotalHour);
			itemsTotalHour = oODataJSONModelTotalHour.getData().results;

			this.calculateProject(0, monTotalHour, projectMonHour, prMonCount, monStatus);
			this.calculateProject(1, tueTotalHour, projectTueHour, prTueCount, tueStatus);
			this.calculateProject(2, wedTotalHour, projectWedHour, prWedCount, wedStatus);
			this.calculateProject(3, thurTotalHour, projectThurHour, prThurCount, thurStatus);
			this.calculateProject(4, friTotalHour, projectFriHour, prFriCount, friStatus);
			this.calculateProject(5, satTotalHour, projectSatHour, prSatCount, satStatus);

			/*double conver to int*/
			if (parseFloat(projectMonHour) !== 0) {
				json.projectMonHour = parseFloat(projectMonHour);
			}
			if (parseFloat(projectTueHour) !== 0) {
				json.projectTueHour = parseFloat(projectTueHour);
			}
			if (parseFloat(projectWedHour) !== 0) {
				json.projectWedHour = parseFloat(projectWedHour);
			}
			if (parseFloat(projectThurHour) !== 0) {
				json.projectThurHour = parseFloat(projectThurHour);
			}
			if (parseFloat(projectFriHour) !== 0) {
				json.projectFriHour = parseFloat(projectFriHour);
			}
			if (parseFloat(projectSatHour) !== 0) {
				json.projectSatHour = parseFloat(projectSatHour);
			}

			json.customerName = customerName;
			json.projectName = projectName;
			json.projectCode = projectCode;
			json.activityNo = activityNo;
			json.worktypeName = worktypeName;

			allData.setData(json);
			this.getView().setModel(allData);

			/* button gosterimi*/
			this.showBtn(projectMonHour, "btnMon8saat", "btnMonDelete", monTotalHour, "monInput", 3);
			this.showBtn(projectTueHour, "btnTue8saat", "btnTueDelete", tueTotalHour, "tueInput", 3);
			this.showBtn(projectWedHour, "btnWed8saat", "btnWedDelete", wedTotalHour, "wedInput", 3);
			this.showBtn(projectThurHour, "btnThur8saat", "btnThurDelete", thurTotalHour, "thurInput", 3);
			this.showBtn(projectFriHour, "btnFri8saat", "btnFriDelete", friTotalHour, "friInput", 3);
			this.showBtn(projectSatHour, "btnSat5saat", "btnSatDelete", satTotalHour, "satInput", 6);

			/*eğer tatilse butonlar disable olmalı*/
			if (disabledDates[0] === true) {
				this.disableBtn("monInput", "btnMon8saat", "btnMonDelete");
			}
			if (disabledDates[1] === true) {
				this.disableBtn("tueInput", "btnTue8saat", "btnTueDelete");
			}
			if (disabledDates[2] === true) {
				this.disableBtn("wedInput", "btnWed8saat", "btnWedDelete");
			}
			if (disabledDates[3] === true) {
				this.disableBtn("thurInput", "btnThur8saat", "btnThurDelete");
			}
			if (disabledDates[4] === true) {
				this.disableBtn("friInput", "btnFri8saat", "btnFriDelete");
			}
			if (disabledDates[5] === true) {
				this.disableBtn("satInput", "btnSat5saat", "btnSatDelete");
			}
		},
		setFocusIssues: function (txtName) {
			var oXInput = sap.ui.getCore().byId(this.createId(txtName));
			oXInput.getFocusDomRef = function () {
				mySelf.onTap(txtName);
				return this.getDomRef().firstChild;
			};
		},
		showLabels: function () {
			var customer = sap.ui.getCore().byId(this.createId("txtCustomer"));
			var projCode = sap.ui.getCore().byId(this.createId("txtProjectCode"));
			var activityNo = sap.ui.getCore().byId(this.createId("txtActivityNo"));
			var projName = sap.ui.getCore().byId(this.createId("txtProjectName"));
			var workType = sap.ui.getCore().byId(this.createId("txtWorktype"));

			if (worktypeCode === Constants.WORK_TYPE_ATTENDANCE_HOURS_CODE) {
				/*burada text lerde proje açıklaması gözükecek
				 */
				if (projectName === "No Assignment ID" || projectName === "") {
					customer.setVisible(false);
					projCode.setVisible(false);
					activityNo.setVisible(false);
					projName.setVisible(false);
					workType.setVisible(true);
				} else {
					customer.setVisible(true);
					projCode.setVisible(true);
					activityNo.setVisible(true);
					projName.setVisible(true);
					workType.setVisible(false);
				}

			} else {
				/*burada text olarak çalışma tipi gösterilecek*/
				customer.setVisible(false);
				projCode.setVisible(false);
				activityNo.setVisible(false);
				projName.setVisible(false);
				workType.setVisible(true);
			}
		},

		calculateProject: function (dCount, dTotalHour, dProjectHour, dPrCount, dStatus) {
			// kullanıcı izinli/tatil değilse
			if (disabledDates[dCount] === false) {
				// gününe ait toplam saat
				for (var i = 0; i < itemsTotalHour.length; i++) {
					if (itemsTotalHour[i].Datefrom.getDate() === weekDates[dCount].getDate()) {
						dTotalHour = parseFloat(itemsTotalHour[i].Recorded);
						if (dCount == 0) {
							monTotalHour = dTotalHour;
						} else if (dCount == 1) {
							tueTotalHour = dTotalHour;
						} else if (dCount == 2) {
							wedTotalHour = dTotalHour;
						} else if (dCount == 3) {
							thurTotalHour = dTotalHour;
						} else if (dCount == 4) {
							friTotalHour = dTotalHour;
						} else if (dCount == 5) {
							satTotalHour = dTotalHour;
						}
						break;
					}
				}

				// günü projeye ait toplam saat
				for (var i = 0; i < itemsProjectHour.length; i++) {
					if (itemsProjectHour[i].Workdate.getDate() === weekDates[dCount].getDate() && itemsProjectHour[i].Rnplnr === projectCode &&
						itemsProjectHour[i].Status !== Constants.WORK_TYPE_STATUS_CODE_CANCELED && itemsProjectHour[i].Status !== Constants.WORK_TYPE_STATUS_CODE_CHANGED_AFTER_APPROVAL &&
						activityNo.includes(itemsProjectHour[i].Vornr) === true && itemsProjectHour[i].Awart === worktypeCode) {
						dProjectHour = parseFloat(itemsProjectHour[i].Catshours);
						dPrCount = itemsProjectHour[i].Counter; //projeye saat girilmişse counterı
						dStatus = itemsProjectHour[i].Status;
						if (dCount == 0) {
							projectMonHour = dProjectHour;
							prMonCount = dPrCount;
							monStatus = dStatus;
						} else if (dCount == 1) {
							projectTueHour = dProjectHour;
							prTueCount = dPrCount;
							tueStatus = dStatus;
						} else if (dCount == 2) {
							projectWedHour = dProjectHour;
							prWedCount = dPrCount;
							wedStatus = dStatus;
						} else if (dCount == 3) {
							projectThurHour = dProjectHour;
							prThurCount = dPrCount;
							thurStatus = dStatus;
						} else if (dCount == 4) {
							projectFriHour = dProjectHour;
							prFriCount = dPrCount;
							friStatus = dStatus;
						} else if (dCount == 5) {
							projectSatHour = dProjectHour;
							prSatCount = dPrCount;
							satStatus = dStatus;
						}
						break;
					}
				}
			}
		},

		disableBtn: function (input, eightBtn, delBtn) {
			var oInput = sap.ui.getCore().byId(this.createId(input));
			oInput.setEnabled(false);
			var oBtnDelete = sap.ui.getCore().byId(this.createId(delBtn));
			oBtnDelete.setEnabled(false);
			var oBtn8saat = sap.ui.getCore().byId(this.createId(eightBtn));
			oBtn8saat.setEnabled(false);
		},

		onKeyChange: function (input, eightBtn, delBtn, totalHour, projectHour) {
			var oInput = sap.ui.getCore().byId(this.createId(input));
			var oInputDel = sap.ui.getCore().byId(this.createId(delBtn));
			var oInputEight = sap.ui.getCore().byId(this.createId(eightBtn));
			var diff;
			if (projectHour === "" || projectHour === null || projectHour === undefined) {
				diff = 11 - parseFloat(totalHour);
			} else {
				diff = 11 - parseFloat(totalHour) + parseFloat(projectHour);
			}
			if (oInput.getValue() !== "" && oInput.getValue() !== null) {
				var inp = parseFloat(oInput.getValue());
				if (inp > diff) {
					this.ShowPopupError(this.getResource("remainingHour") + " " + parseFloat(diff) + " " + this.getResource("hourEnter"));
					oInput.setValue("");
					oInput._lastValue = null;
					if (eightBtn === "btnSat5saat") {
						this.visibilityBtnForDiffSat(diff, oInputEight, oInputDel);
					} else {
						this.visibilityBtnForDiff(diff, oInputEight, oInputDel);
					}
				} else {
					oInputEight.setVisible(false);
					oInputDel.setVisible(true);
				}
			} else {
				if (eightBtn === "btnSat5saat") {
					this.visibilityBtnForDiffSat(diff, oInputEight, oInputDel);
				} else {
					this.visibilityBtnForDiff(diff, oInputEight, oInputDel);
				}
			}
		},

		numberOrCommaCheck: function (evt) {
			var theEvent = evt || window.event;
			var key = theEvent.key || theEvent.which;
			key = String.fromCharCode(key);
			var regex = /^[0-9,]+$/;
			if (!regex.test(key)) {
				theEvent.returnValue = false;
				if (theEvent.preventDefault) theEvent.preventDefault();
			}
		},

		visibilityBtnForDiff: function (diff, eightBtn, delBtn) {
			if (diff >= 8) {
				eightBtn.setVisible(true);
				eightBtn.setEnabled(true);
				delBtn.setVisible(false);
			} else {
				eightBtn.setVisible(true);
				eightBtn.setEnabled(false);
				delBtn.setVisible(false);
			}
		},
		visibilityBtnForDiffSat: function (diff, fiveBtn, delBtn) {
			if (diff >= 5) {
				fiveBtn.setVisible(true);
				fiveBtn.setEnabled(true);
				delBtn.setVisible(false);
			} else {
				fiveBtn.setVisible(true);
				fiveBtn.setEnabled(false);
				delBtn.setVisible(false);
			}
		},

		onKeyChangeMon: function (oEvent) {
			this.numberOrCommaCheck(oEvent);
			return this.onKeyChange("monInput", "btnMon8saat", "btnMonDelete", monTotalHour, projectMonHour);
		},
		onKeyChangeTue: function (oEvent) {
			this.numberOrCommaCheck(oEvent);
			return this.onKeyChange("tueInput", "btnTue8saat", "btnTueDelete", tueTotalHour, projectTueHour);
		},
		onKeyChangeWed: function (oEvent) {
			this.numberOrCommaCheck(oEvent);
			return this.onKeyChange("wedInput", "btnWed8saat", "btnWedDelete", wedTotalHour, projectWedHour);
		},
		onKeyChangeThur: function (oEvent) {
			this.numberOrCommaCheck(oEvent);
			return this.onKeyChange("thurInput", "btnThur8saat", "btnThurDelete", thurTotalHour, projectThurHour);
		},
		onKeyChangeFri: function (oEvent) {
			this.numberOrCommaCheck(oEvent);
			return this.onKeyChange("friInput", "btnFri8saat", "btnFriDelete", friTotalHour, projectFriHour);
		},
		onKeyChangeSat: function (oEvent) {
			this.numberOrCommaCheck(oEvent);
			return this.onKeyChange("satInput", "btnSat5saat", "btnSatDelete", satTotalHour, projectSatHour);
		},

		showBtn: function (projectHour, eightBtn, delBtn, totalHour, input, count) {
			var oInput = sap.ui.getCore().byId(this.createId(input));
			var oInputDel = sap.ui.getCore().byId(this.createId(delBtn));
			var oInputEightOrFive = sap.ui.getCore().byId(this.createId(eightBtn));
			//cache de kalıyor disable edilmesi o yüzden en baştan enable ediyoruz.
			oInput.setEnabled(true);
			oInput.setPlaceholder(this.getResource("enterHour"));
			if (projectHour === "" || projectHour === null || projectHour === undefined || projectHour === 0 || projectHour === "0") {
				oInputDel.setVisible(false);
				oInputEightOrFive.setVisible(true);
				if (totalHour <= count) {
					oInputEightOrFive.setEnabled(true);
				} else {
					oInputEightOrFive.setEnabled(false);
					if (totalHour === 11) {
						oInput.setEnabled(false);
						oInput.setPlaceholder("");
					}
				}
			} else {
				oInputEightOrFive.setVisible(false);
				oInputDel.setVisible(true);
				oInput.setEnabled(true);
			}
		},

		onTap: function (name) {
			focusedDay = name;
		},
		/* inputun içini temizleme, saati 0 lama 
		 */
		deletePress: function (parameterInput, projectHour, eightBtn, delBtn, totalHour) {
			var oInput = sap.ui.getCore().byId(this.createId(parameterInput));
			var oInputDel = sap.ui.getCore().byId(this.createId(delBtn));
			var oInputEight = sap.ui.getCore().byId(this.createId(eightBtn));
			var diff;
			if (projectHour !== "" && projectHour !== null && projectHour !== undefined) {
				diff = 11 - parseInt(totalHour, 10) + parseInt(projectHour, 10);
			} else {
				diff = 11 - parseInt(totalHour, 10);
			}
			if (eightBtn === "btnSat5saat") {
				this.visibilityBtnForDiffSat(diff, oInputEight, oInputDel);
			} else {
				this.visibilityBtnForDiff(diff, oInputEight, oInputDel);
			}
			oInput._lastValue = null;
			oInput.setValue("");
			oInput.setEnabled(true);
			if (focusedDay === parameterInput) {
				oInput.focus();
			}
		},

		eightPress: function (parameterInput, eightBtn, delBtn) {
			var oInputDel = sap.ui.getCore().byId(this.createId(delBtn));
			var oInputEight = sap.ui.getCore().byId(this.createId(eightBtn));
			var oInput = sap.ui.getCore().byId(this.createId(parameterInput));
			oInput._lastValue = "8";
			oInput.setValue("8");
			oInputEight.setVisible(false);
			oInputDel.setVisible(true);
			focusedDay = ""; //parameterInput;
		},

		fivePress: function (parameterInput, fiveBtn, delBtn) {
			var oInputDel = sap.ui.getCore().byId(this.createId(delBtn));
			var oInputEight = sap.ui.getCore().byId(this.createId(fiveBtn));
			var oInput = sap.ui.getCore().byId(this.createId(parameterInput));
			oInput._lastValue = "5";
			oInput.setValue("5");
			oInputEight.setVisible(false);
			oInputDel.setVisible(true);
		},

		monDeletePress: function (evt) {
			return this.deletePress("monInput", projectMonHour, "btnMon8saat", "btnMonDelete", monTotalHour);
		},
		tueDeletePress: function (evt) {
			return this.deletePress("tueInput", projectTueHour, "btnTue8saat", "btnTueDelete", tueTotalHour);
		},
		wedDeletePress: function (evt) {
			return this.deletePress("wedInput", projectWedHour, "btnWed8saat", "btnWedDelete", wedTotalHour);
		},
		thurDeletePress: function (evt) {
			return this.deletePress("thurInput", projectThurHour, "btnThur8saat", "btnThurDelete", thurTotalHour);
		},
		friDeletePress: function (evt) {
			return this.deletePress("friInput", projectFriHour, "btnFri8saat", "btnFriDelete", friTotalHour);
		},
		satDeletePress: function (evt) {
			return this.deletePress("satInput", projectSatHour, "btnSat5saat", "btnSatDelete", satTotalHour);
		},

		sat5saatPress: function (evt) {
			return this.fivePress("satInput", "btnSat5saat", "btnSatDelete");
		},
		fri8saatPress: function (evt) {
			return this.eightPress("friInput", "btnFri8saat", "btnFriDelete");
		},
		thur8saatPress: function (evt) {
			return this.eightPress("thurInput", "btnThur8saat", "btnThurDelete");
		},
		wed8saatPress: function (evt) {
			return this.eightPress("wedInput", "btnWed8saat", "btnWedDelete");
		},
		tue8saatPress: function (evt) {
			return this.eightPress("tueInput", "btnTue8saat", "btnTueDelete");
		},
		mon8saatPress: function (evt) {
			return this.eightPress("monInput", "btnMon8saat", "btnMonDelete");
		},

		handleSuccessMessageBoxPress: function () {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.success(
				"{i18n>operationSuccessful}", {
					styleClass: bCompact ? "sapUiSizeCompact" : ""
				}
			);
		},

		/* save/update/delete function
		 */
		savePress: function (evt) {
			this.showBusyIndicator(4000, 0);
			var oMonInput = sap.ui.getCore().byId(this.createId("monInput"));
			var enteredMonHour = oMonInput.getValue();
			var oTueInput = sap.ui.getCore().byId(this.createId("tueInput"));
			var enteredTueHour = oTueInput.getValue();
			var oWedInput = sap.ui.getCore().byId(this.createId("wedInput"));
			var enteredWedHour = oWedInput.getValue();
			var oThurInput = sap.ui.getCore().byId(this.createId("thurInput"));
			var enteredThurHour = oThurInput.getValue();
			var oFriInput = sap.ui.getCore().byId(this.createId("friInput"));
			var enteredFriHour = oFriInput.getValue();
			var oSatInput = sap.ui.getCore().byId(this.createId("satInput"));
			var enteredSatHour = oSatInput.getValue();
			var errorDays = "";
			var errorMessage = "";
			var successMessage = this.getResource("operationSuccessful");
			try {
				this.saveMon(enteredMonHour);
			} catch (err) {
				errorDays = errorDays + this.getResource("monday") + " ";
				errorMessage = err.response.body.split(':')[5].split('}')[0]; //err.response.body.message.value değerini alıyoruz sadece
			}
			try {
				this.saveTue(enteredTueHour);
			} catch (err) {
				errorDays = errorDays + this.getResource("tuesday") + " ";
				errorMessage = err.response.body.split(':')[5].split('}')[0]; //err.response.body.message.value değerini alıyoruz sadece
			}
			try {
				this.saveWed(enteredWedHour);
			} catch (err) {
				errorDays = errorDays + this.getResource("wednesday") + " ";
				errorMessage = err.response.body.split(':')[5].split('}')[0]; //err.response.body.message.value değerini alıyoruz sadece	
			}
			try {
				this.saveThur(enteredThurHour);
			} catch (err) {
				errorDays = errorDays + this.getResource("thursday") + " ";
				errorMessage = err.response.body.split(':')[5].split('}')[0]; //err.response.body.message.value değerini alıyoruz sadece		
			}
			try {
				this.saveFri(enteredFriHour);
			} catch (err) {
				errorDays = errorDays + this.getResource("friday") + " ";
				errorMessage = err.response.body.split(':')[5].split('}')[0]; //err.response.body.message.value değerini alıyoruz sadece	
			}
			try {
				this.saveSat(enteredSatHour);
			} catch (err) {
				errorDays = errorDays + this.getResource("saturday") + " ";
				errorMessage = err.response.body.split(':')[5].split('}')[0]; //err.response.body.message.value değerini alıyoruz sadece	
			}

			if (errorDays === "") { ///herhangi bir günle ilgili işlemde hata alınmaz ise
				this.NavigateToView("timeSheetSummary");
				MessageToast.show(successMessage, {
					animationDuration: "3000",
					width: "100%",
					at: {
						LeftTop: 0
					}
				});
			} else { ///işlemler sırasında hata alınan günler
				var errorCRTD = this.getResource("errorCRTD");
				var errorTECO = this.getResource("errorTECO");
				var errorCLSD = this.getResource("errorCLSD");
				var errorProfitCenterFirst = this.getResource("empAssign");
				var errorProfitCenterSecond = this.getResource("diffFunct");
				if (errorMessage.includes(errorCRTD) === true) {
					this.ShowPopupError(this.getResource("registrationError") + " " + this.getResource("activeCRTD") + " " + this.getResource(
						"unsavedDays") + ": " + errorDays);
				} else if (errorMessage.includes(errorTECO) === true) {
					this.ShowPopupError(this.getResource("registrationError") + " " + this.getResource("activeTECO") + " " + this.getResource(
						"unsavedDays") + ": " + errorDays);
				} else if (errorMessage.includes(errorCLSD) === true) {
					this.ShowPopupError(this.getResource("registrationError") + " " + this.getResource("activeCLSD") + " " + this.getResource(
						"unsavedDays") + ": " + errorDays);
				} else if (errorMessage.includes(errorProfitCenterFirst) === true && errorMessage.includes(errorProfitCenterSecond) === true) {
					this.ShowPopupError(this.getResource("registrationError") + " " + this.getResource("profitCenter") + " " + this.getResource(
						"unsavedDays") + ": " + errorDays);
				} else {
					this.ShowPopupError(this.getResource("registrationError") + " " + errorMessage + " " + this.getResource("connectIT") + " " + this
						.getResource("unsavedDays") + ": " + errorDays);
				}
			}
		},

		saveDay: function (dPrCount, index, enteredHour, dStatus, dTotalHour, dProjectHour) {
			//proje için pazartesi gününe önceden girilmiş kayıt var mı?
			if (dPrCount !== null) { //update or delete
				var url = "ActivitySet(Pernr='" + Common.PersonnelNo() + "',Workdate=" + Common.DateToString(weekDates[index], true) +
					",Counter='" + dPrCount + "')"; //update için
				//bu güne girilmiş saat var o zaman update edicez
				if (enteredHour !== "" && enteredHour !== null) { //eğer yapılan değişiklik sonucu girilen input boşaltılmamışsa update
					if (dStatus !== Constants.WORK_TYPE_STATUS_CODE_CANCELED) {
						var ifUpdate = parseFloat(dTotalHour) - parseFloat(dProjectHour) + parseFloat(enteredHour);
						if (ifUpdate <= 11) {
							var oEntry = Common.CreateEntry(Common.PersonnelNo(), Common.DateToISODateTime(weekDates[index]), enteredHour, "update",
								projectCode, activityNo, worktypeCode);
							oEntry.Counter = dPrCount;
							Common.UpdateItem(url, oEntry);
						}
					}
				} else if (dStatus !== Constants.WORK_TYPE_STATUS_CODE_CANCELED && dStatus !== Constants.WORK_TYPE_STATUS_CODE_CHANGED_AFTER_APPROVAL) {
					Common.DeleteItem(url); //boşaltılmışsa delete işlemi yapılacak
				}
			} else {
				//bugüne daha önce giriş yapılmamış insert edilecek.
				//bu güne girilmiş saat var mı?
				if (enteredHour !== "" && enteredHour !== null) { //eğer yapılan değişiklik sonucu girilen input boşaltılmamışsa insert
					var urlinsert = "ActivitySet";
					var ifinsert = parseFloat(dTotalHour) + parseFloat(enteredHour);
					if (ifinsert <= 11) {
						var oEntryIns = Common.CreateEntry(Common.PersonnelNo(), Common.DateToISODateTime(weekDates[index]), enteredHour, "create",
							projectCode, activityNo, worktypeCode);
						Common.InsertItem(urlinsert, oEntryIns);
					}
				}
				//boşaltılmışsa hiç bişey yapılmaz zaten kayıt yoktu öncesinde
			}
		},

		saveMon: function (enteredMonHour) {
			this.saveDay(prMonCount, 0, enteredMonHour, monStatus, monTotalHour, projectMonHour);
		},

		saveTue: function (enteredTueHour) {
			this.saveDay(prTueCount, 1, enteredTueHour, tueStatus, tueTotalHour, projectTueHour);
		},

		saveWed: function (enteredWedHour) {
			this.saveDay(prWedCount, 2, enteredWedHour, wedStatus, wedTotalHour, projectWedHour);
		},

		saveThur: function (enteredThurHour) {
			this.saveDay(prThurCount, 3, enteredThurHour, thurStatus, thurTotalHour, projectThurHour);
		},

		saveFri: function (enteredFriHour) {
			this.saveDay(prFriCount, 4, enteredFriHour, friStatus, friTotalHour, projectFriHour);
		},

		saveSat: function (enteredSatHour) {
			this.saveDay(prSatCount, 5, enteredSatHour, satStatus, satTotalHour, projectSatHour);
		},

		backToPage: function () {
			return this.onNavBack();
		},

		onPressCancel: function () {
			return this.NavigateToView("timeSheetSummary");
		}

	});
});