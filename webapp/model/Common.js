sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"com/deloitte/timesheet/model/Constants"
], function(JSONModel, Constants) {
	"use strict";

	return {
		setDataToModel: function(dataToStore, modelName) {
			var myModel = new sap.ui.model.json.JSONModel();
			myModel.setData(dataToStore);

			sap.ui.getCore().setModel(myModel, modelName);
		},
		getDataFromModel: function(modelName) {
			var data = sap.ui.getCore().getModel(modelName);
			if (data) {
				return data.getData();
			}
			else {
				return data; //yani undefined return et.
			}
		},
		LoadPersonnelInfoSAP: function() {
			var url = "getPersonInfoBySysuser?Username=%27%27";
			var oODataJSONModel = this.CallSAPMethod(url);
			//var oValue = sap.ui.getCore().getModel().getProperty("/oSelectedValue"); //Get the Property Value
			//sap.ui.getCore().getModel().setProperty("/oSelectedValue", oValue); //Overwrite property value (or) set the property value again
			this.setDataToModel(oODataJSONModel.getData().Perno, Constants.PERSONNEL_MODEL);
		},
		PersonnelNo: function() {
			return this.getDataFromModel(Constants.PERSONNEL_MODEL); //mertin personel no  "90000045"; 
		},
		DateToString: function(date, forSapServiceParamater) {
			//var s = "datetime'2016-01-01T00%3A00%3A00'";
			var returnValue = this.FormatToDate(date)+ "T00%3A00%3A00";
			if (forSapServiceParamater) {
				returnValue = "datetime'" + returnValue + "'";
			}
			return returnValue;
		},
		
		FormatToDate: function(date){
			//var s = "2016-01-01";
			return date.getFullYear() + "-" + ((date.getMonth() + 1) <= 9 ? "0" : "") + (date.getMonth() + 1) + "-" + (date.getDate() <= 9 ? "0" : "") + date.getDate();
		},
		FormatToDateAndTime: function(date){
			//var s = "2016-01-01T12:12:11";
			return date.getFullYear() + "-" + ((date.getMonth() + 1) <= 9 ? "0" : "") + (date.getMonth() + 1) + "-" + (date.getDate() <= 9 ? "0" : "") + date.getDate()+"T"+(date.getHours() <= 9 ? ("0"+date.getHours()) : date.getHours())+":"+(date.getMinutes() <= 9 ? ("0"+date.getMinutes()) : date.getMinutes())+":"+(date.getSeconds() <= 9 ? ("0"+date.getSeconds()) : date.getSeconds());
		},
		OnlyDateShow: function(date) {
			//var s = "01.01.2016";
			 return (date.getDate() <= 9 ? "0" : "") + date.getDate() + "." + ((date.getMonth() + 1) <= 9 ? "0" : "") + (date.getMonth() + 1) + "."  + date.getFullYear();
		},
		OnlyDayMonthShow: function(date) {
			//var s = "01.01.2016";
			 return (date.getDate() <= 9 ? "0" : "") + date.getDate() + "." + ((date.getMonth() + 1) <= 9 ? "0" : "") + (date.getMonth() + 1);
		},
		DateToISODateTime: function(date) {
			//var s = "2016-01-01T00:00:00";
	     	return this.FormatToDate(date) + "T00:00:00";
		},
		CallSAPMethod: function(url) {
			var oODataJSONModel = null;
		    var oModelY = this.SetDomainModelComponent();
		    // oModelY=new sap.ui.model.odata.ODataModel(Constants.SAP_SERVICE_URL);
			// insert url parameters
			oModelY.read(url,
				null,
				null,
				false,
				function(oData, oResponse) {
					// create JSON model
					oODataJSONModel = new sap.ui.model.json.JSONModel();
					// set the odata JSON as data of JSON model
					oODataJSONModel.setData(oData);
				});
			return oODataJSONModel;
		},
		CallSAPMethodasynch: function(url, readData) {
			var mem = this;
			var oModelY = this.SetDomainModelComponent();
			// insert url parameters
			oModelY.read(url,
				null,
				null,
				true,
				function(oData, oResponse) {
					mem.setDataToModel(oData, Constants.ASYNCHRONOUS_DATA_RETURND);
					readData();
					return oData;
				});
		},
	    SetDomainModelComponent:function(){
    			// Create and set domain model to the component
				var oModelY = new sap.ui.model.odata.ODataModel(Constants.SAP_SERVICE_URL, {
					json: true,
					loadMetadataAsync: true
				});
				return oModelY;
    	},
		DeleteItem:function(url){
				var oModelY=this.SetDomainModelComponent();
				// insert url parameters
				oModelY.remove(url, {
	    		method: "DELETE",
				success: function() {
				 },
				error: function(err) {
	    			  throw err;}
				});
    	},
    	
    	UpdateItem:function(url,oEntry){
    			var oModelY=this.SetDomainModelComponent();
				// insert url parameters
				oModelY.update(url, oEntry, {
	    		method: "PUT",
				success: function() {
				 },
				 error: function(err) {
	    			 throw err;}
				});
    	},
    	InsertItem:function(url,oEntry){
	           var oModelY=this.SetDomainModelComponent();
	            
				// insert url parameters
				oModelY.create(url, oEntry, {
	    		method: "POST",
	    		success: function() {
				 },
				error: function(err) {
	    			  throw err;}
				});
    	},
    	CreateEntry:function(pernr,workdate,catshour,ltxa1,rnplnr,vornr,awart){
    		   if(vornr.includes("(")===true && vornr.includes(")")===true){
    		   	   vornr=vornr.split('(')[1].split(')')[0];
    		   }
				var newDate = new Date();
			    var today = new Date(newDate.getTime()); //bugün
			    var ers=this.FormatToDateAndTime(today);
				var oEntry = {};
				oEntry.Pernr = pernr;
				oEntry.Workdate = workdate;
				oEntry.Catshours = catshour;
			    oEntry.Ltxa1= ltxa1;  		
    		    oEntry.Rnplnr=rnplnr;
    		    oEntry.Vornr=vornr;
    		    oEntry.Awart=awart;
    		    oEntry.Ersda=ers;
    		    return oEntry;
		}
   };
}
);