sap.ui.define([
	"sap/ui/core/mvc/Controller",
	'sap/ui/unified/DateTypeRange',
	'sap/ui/unified/DateRange',
	"com/deloitte/timesheet/controller/BaseController",
	'sap/ui/model/json/JSONModel',
	'sap/ui/model/Filter',
	'com/deloitte/timesheet/model/Constants',
	'com/deloitte/timesheet/model/Common'
], function(Controller, DateTypeRange, DateRange, BaseController, JSONModel, Filter, Constants, Common) {
	"use strict";
     return BaseController.extend("com.deloitte.timesheet.controller.searchWorkType", {
	
    	onInit: function() {
			this.getRouter().attachRouteMatched(this.handleRouteMatched, this);	
		},
	
		handleRouteMatched: function(evt) {
				if (evt.getParameter("name") !== "searchWorkType") {
					return;
				}
				this.showBusyIndicator(3000,0);
				sap.ui.getCore().byId(this.createId("searchField")).setValue(""); //clear search history
				var url = "PersonSet('" + Common.PersonnelNo() + "')//AbsenceAttendanceSet";
				var oODataJSONModel = Common.CallSAPMethod(url);
				this.getView().setModel(oODataJSONModel);
		},
		onSearch: function(oEvt) {
			var aFilters = [];
			var sQuery = oEvt.getSource().getValue();
			if (sQuery && sQuery.length > 0) {
				var filter = new Filter("Atext", sap.ui.model.FilterOperator.Contains, sQuery);
				aFilters.push(filter);
			}

			// update list binding
			var list = sap.ui.getCore().byId(this.createId("WorkTypeList"));
		    var binding = list.getBinding("items");
			binding.filter(aFilters);
		},
		
		selectWorkType: function(oEvt) {
				var oItem = oEvt.getSource();
				// The model that is bound to the item
				var oContext = oItem.getBindingContext();

				var workTypeCode = oContext.getProperty("Awart");
				var workTypeName = oContext.getProperty("Atext");

				Common.setDataToModel(workTypeCode, Constants.TIMESHEET_ENTRY_WORK_TYPE_CODE);
				Common.setDataToModel(workTypeName, Constants.TIMESHEET_ENTRY_WORK_TYPE_NAME);

				if (workTypeCode === Constants.WORK_TYPE_ATTENDANCE_HOURS_CODE) {
					this.NavigateToView("searchProject");
				} else {
					Common.setDataToModel("", Constants.TIMESHEET_ENTRY_PROJECT_CODE);
					Common.setDataToModel("", Constants.TIMESHEET_ENTRY_PROJECT_NAME);
					Common.setDataToModel("", Constants.TIMESHEET_ENTRY_ACTIVITY_NO);
					
					Common.setDataToModel("", Constants.TIMESHEET_ENTRY_CUSTOMER_CODE); //servise eklenince gönderilecek.
					Common.setDataToModel("", Constants.TIMESHEET_ENTRY_CUSTOMER_NAME);
					this.NavigateToView("projectTimePage");
			}
		},
		 
	    onPressCancel:function(){
			return this.NavigateToView("timeSheetSummary");
		}
	});

});