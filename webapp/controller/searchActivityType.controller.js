sap.ui.define([
	"sap/ui/core/mvc/Controller",
	'sap/ui/unified/DateTypeRange',
	'sap/ui/unified/DateRange',
	"com/deloitte/timesheet/controller/BaseController",
	'sap/ui/model/json/JSONModel',
	'sap/ui/model/Filter',
	'com/deloitte/timesheet/model/Constants',
	'com/deloitte/timesheet/model/Common'
], function(Controller, DateTypeRange, DateRange, BaseController, JSONModel, Filter, Constants, Common) {
	"use strict";
    var oItem=null;
	return BaseController.extend("com.deloitte.timesheet.controller.searchActivityType", {

		onInit: function() {
			this.getRouter().attachRouteMatched(this.handleRouteMatched, this);	
		},
		 
		handleRouteMatched: function(evt) {
				if (evt.getParameter("name") !== "searchActivityType") {
					return;
				}
				this.showBusyIndicator(3000,0);
				sap.ui.getCore().byId(this.createId("searchField")).setValue(""); //clear search history
				oItem=null;
				var projectCode = Common.getDataFromModel(Constants.TIMESHEET_ENTRY_PROJECT_CODE);
				var url = "AssignmentSet('" + projectCode + "')//TaskSet";
				var oODataJSONModel = Common.CallSAPMethod(url);
				this.getView().setModel(oODataJSONModel);
		},
		
		ShowSearchedData: function() {
			var oData = Common.getDataFromModel(Constants.ASYNCHRONOUS_DATA_RETURND);
			var me = Common.getDataFromModel(Constants.CURRENT_CONTROLLER);
			var oODataJSONModel = new sap.ui.model.json.JSONModel();
			// create JSON model
			// set the odata JSON as data of JSON model
			oODataJSONModel.setData(oData);
			var busycontrol = sap.ui.getCore().byId(me.createId("isBusy"));
			//busycontrol.style.visibility = 'hidden';
			busycontrol.setVisible(false);
			me.getView().setModel(oODataJSONModel);
			return oODataJSONModel;

		},

		onSearch: function(oEvt) {

			// add filter for search
			/*var count = this.getView().byId("AssignmentList").getItems().length;
			var assigmentList = sap.ui.getCore().byId(this.createId("AssignmentList"));
			if (count > 0) {
				assigmentList.;
			}*/
			var aFilters = [];
			var sQuery = oEvt.getSource().getValue();
			if (sQuery && sQuery.length > 0) {
				var filter = new Filter("Vornr", sap.ui.model.FilterOperator.Contains, sQuery);
				aFilters.push(filter);
			}

			// update list binding
			var list = sap.ui.getCore().byId(this.createId("ActivityTypeList"));
			var binding = list.getBinding("items");
			binding.filter(aFilters);
		},

		// update list binding
		/*var list = this.getView().byId("AssignmentList");
		var binding = list.getBinding("items");
		binding.filter(aFilters, "Application");*/

		selectActivity: function(oEvt) {
			    oItem = oEvt.getSource();
				// The model that is bound to the item
				var oContext = oItem.getBindingContext();

				var activityCode = oContext.getProperty("Vornr");
				activityCode="("+activityCode+")";
				Common.setDataToModel(activityCode, Constants.TIMESHEET_ENTRY_ACTIVITY_NO);

				this.NavigateToView("projectTimePage");
			},
			 
	    onPressCancel:function(){
			return this.NavigateToView("timeSheetSummary");
		}
			//PersonSet('90000090')/AssignmentSet
			/**
			 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
			 * (NOT before the first rendering! onInit() is used for that one!).
			 * @memberOf com.deloitte.timesheet.view.searchProject
			 */
			//	onBeforeRendering: function() {
			//
			//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.deloitte.timesheet.view.searchProject
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.deloitte.timesheet.view.searchProject
		 */
		//	onExit: function() {
		//
		//	}

	});

});