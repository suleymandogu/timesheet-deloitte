sap.ui.define([
	'sap/ui/unified/CalendarLegendItem',
	'sap/ui/unified/DateTypeRange',
	'sap/ui/unified/DateRange',
	"com/deloitte/timesheet/controller/BaseController",
	'sap/ui/model/json/JSONModel',
	'com/deloitte/timesheet/model/Constants',
	'com/deloitte/timesheet/model/Common',
	'sap/suite/ui/commons/util/DateUtils',
	'sap/m/MessageToast'
], function(CalendarLegendItem, DateTypeRange, DateRange, BaseController, JSONModel, Constants, Common, DateUtils, MessageToast) {
	"use strict";

	return BaseController.extend("com.deloitte.timesheet.controller.weekView", {
		getMinDateReal: function(todayR) {
			var today = new Date(todayR.getFullYear(),todayR.getMonth(), todayR.getDate()); // to remove time info
			var first = new Date(today.getTime() - (today.getDay() - 1) * 86400000);
			var minDate = new Date(first.getTime() - (12 * 7 * 86400000)); //this is real min date
			return minDate;
		},
		getMinDate: function (minDateReal) {
			var minDate = new Date(minDateReal.getFullYear(),minDateReal.getMonth(),1); 
			return new Date(minDate.getTime() - (minDate.getDay() - 1) * 86400000);
		},
		getMaxDateReal: function(today) { //günün içinde bulunduğu ay ve bir sonraki ayın son gününe kadar. 1 şubat ise bugün, max date real 31 mart, 20 şubat ise bugün, max date real gene 31 mart
			var maxDate = DateUtils.incrementMonthByIndex(new Date(today.getFullYear(),today.getMonth(),1),2);
			maxDate = DateUtils.incrementDateByIndex(maxDate, -1);
			return maxDate;
		},
		getMaxDate: function(today) { //son günün içinde bulunduğu haftanın hepsini içeren max date, Calendar kontrolü bunu maxdate olarak kullanıyor.
			var maxDate = this.getMaxDateReal(today);
			var firstMaxDate = new Date(maxDate.getTime() - (maxDate.getDay() - 1) * 86400000);
			maxDate = new Date(firstMaxDate.getTime() + (5 * 86400000));
			return maxDate;
		},
		setCalendarView: function(bShowLegends) {
			//tatil ve izin günlerini çek ve disabled date'leri ayarla
			//her gün için çalışılan toplam saati bul ve legend'leri ayarla
			var oCalendar = sap.ui.getCore().byId(this.createId("calendarw"));
			oCalendar.removeAllSpecialDates(); 
			this.oFormatYyyymmdd = sap.ui.core.format.DateFormat.getInstance({
				style: "long"
			});
			var today = new Date();
			var minDateReal = this.getMinDateReal(today); //this is real min date
			//min date is made the 1st of the minDate's month below, so that
			//when user clicks the dates between real mindate and the one below, user will get a message you are allowed only 13 weeks ago.. 
			var minDate = this.getMinDate(minDateReal);
			
			//var maxDate = new Date(first.getTime() + (13 * 86400000));
			var maxDate = this.getMaxDate(today);
			Constants.CALENDAR_MAX_DATE=maxDate; //takvimde gösterilen son tarih
			
			var months = 1; //this.monthDiff(maxDattodaye, minDate);

			//var disabledDates = [];

			var oModel = new JSONModel();
			oModel.setData({
				minDate: minDate,
				maxDate: maxDate,
				months: months,
				disabledDates: [{
					/*startDate: new Date(2016, 11, 4),
				}, {
					startDate: new Date(2016, 11, 15)*/
				}]
			});

			this.getView().setModel(oModel);

			var lastSelectedDate = Common.getDataFromModel(Constants.PAGE_WEEKVIEW_SELECTEDDATE);

			if (bShowLegends) {
				if (lastSelectedDate) {
					oCalendar.focusDate(lastSelectedDate);
					if (lastSelectedDate > this.getMaxDateReal(new Date())) {
						oCalendar.displayDate(this.getMaxDateReal(new Date()));
					}
				} else {
					oCalendar.focusDate(today);
					oCalendar.displayDate(today);
				}
				//oCalendar.displayDate(new Date(2016, 11, 23));
			} else {
				oCalendar.focusDate(maxDate);
				oCalendar.displayDate(maxDate);
			}
			var disabledDatesCount = 0;
			var disabledDates = [];
			var disabledDatesTypeName = [];
			if (bShowLegends) {
				var url = "TimeSheetSet(Pernr='" + Common.PersonnelNo() + "',Datefrom=" + Common.DateToString(minDate, true) + ",Dateto=" + Common
					.DateToString(maxDate, true) + ")/TimeSheetDateSet";
				var oODataJSONModel = Common.CallSAPMethod(url);
				var datum = oODataJSONModel.getData().results;

				for (var i = 0; i <= datum.length - 1; i++) {
					datum[i].Datefrom = new Date(datum[i].Datefrom);
					datum[i].Dateto = new Date(datum[i].Dateto);
					var oRefDate = datum[i].Datefrom;
					var sType = "";
					//Type01 : /*yeşil renkli, yani tüm gün için giriş yapılmış*/
					//Type02: /*kırmızı renkli, yani tüm gün için giriş yapılmmaış*/
					//Type03: /*sarı renkli, yani tüm gün için giriş yapılmamış ama bir kısmı için yapılmış*/
					if (parseFloat(datum[i].Target) === parseFloat("0.00")) { //giriş yapılması gereken saat sıfır ise
						if (parseFloat(datum[i].Recorded) > parseFloat("0.00")) { // giriş yapılmış ise
							sType = "Type01"; //yeşil
						}
					} else {
						if (parseFloat(datum[i].Recorded) >= parseFloat(datum[i].Target)) { //giriş, giriş yapılması gereken saatten büyük eşit ise
							sType = "Type01"; //yeşil -- giriş yapılması gereken saat sıfırdan büyük ve girilmesi gereken saat kadar giriş yapılmış
						} else {
							if (parseFloat(datum[i].Recorded) > parseFloat("0.00")) { //giriş sıfırdan büyük ise
								//sType = "Type03"; //sarı --giriş yapılması gereken saat sıfırdan büyük ve partial giriş yapılmış
								sType = "Type03"; //kırmızı -- giriş yapılması gereken saat sıfırdan büyük ve hiç giriş yapılmamış
							} else {
								sType = "Type02"; //kırmızı -- giriş yapılması gereken saat sıfırdan büyük ve hiç giriş yapılmamış
							}
						}
					}
					if (datum[i].DayFlag === 2) { //tatil günü
						if (datum[i].HolidayId !== "") { //global tarih
							/*oCalendar.addDisabledDate(new DateRange({
								startDate: oRefDate
							}));*/
							disabledDates[disabledDatesCount] =  oRefDate;
							disabledDatesTypeName[disabledDatesCount] = datum[i].HolidayLt;
							disabledDatesCount++;
						}
						else {
							disabledDates[disabledDatesCount] =  oRefDate;
							disabledDatesTypeName[disabledDatesCount] = this.getResource("holiday");
							disabledDatesCount++;
						}
					}
					else if (datum[i].DayFlag === 1 && parseFloat(datum[i].Target) === parseFloat("0.00")) { //kişisel absence, çalışma günü ama target 0
						disabledDates[disabledDatesCount] =  oRefDate;
						disabledDatesTypeName[disabledDatesCount] = datum[i].Atext;
						disabledDatesCount++;
					}
					else {
						if (sType !== "" && oRefDate >= minDateReal) {
							oCalendar.addSpecialDate(new DateTypeRange({
								startDate: new Date(oRefDate),
								type: sType,
								tooltip: datum[i].Recorded
							}));
						}
					}

				}
				this.startDateChangeOfCalendar();
				
				var json = {};
				
				json.disabledDates = disabledDates;
				json.disabledDatesTypeName = disabledDatesTypeName;
				Common.setDataToModel(json, Constants.DISABLEDDATES_MODEL);
				
				/*var json = {};
				json.selectedDate = dates;
				Common.setDataToModel(json, Constants.TIMESHEET_ENTRY_PERIOD);*/
				
			}
		},
		monthDiff: function(d2, d1) {
			var months;
			months = (d2.getFullYear() - d1.getFullYear()) * 12;
			months -= d1.getMonth();
			months += d2.getMonth() + 1;
			months = d2.getFullYear() - d1.getFullYear() !== 0 ? months - 1 : months;
			return months <= 0 ? 0 : months;
		},

		handleRouteMatched: function(evt) {
			//Check whether is the detail page is matched.
			if (evt.getParameter("name") !== "weekView") {
				return;
			}
			this.showBusyIndicator(2000,0);
			//You code here to run every time when your detail page is called.
			
			this.setCalendarView(true);
			
		
			
			
		},
		weekSelect: function() {
				this.showBusyIndicator(2000,0);
			var json = {};
			var i = 0;
			var j = 0;
			var dates = [];
			var disabledDates = [];
			var disabledDatesTypeName = [];
			
			var calendarControl = sap.ui.getCore().byId(this.createId("calendarw"));
			//var selectedDate = sap.me.Calendar.parseDate(calendarControl.getSelectedDates()[0]);
			var selectedDate = calendarControl.getSelectedDates()[0].getStartDate();
			
			var minDateReal = this.getMinDateReal(new Date()); //this is real min date
			var minDate = this.getMinDate(minDateReal);
			if (minDateReal !== minDate) {
				if(selectedDate >= minDate && selectedDate < minDateReal) {
					this.hideBusyIndicator();
					this.ShowPopupError(this.getResource("error13Week")); 
					return;
				}
			}
			
			//ar dayOfMonth = selectedDate.getDate() - selectedDate.getDay() + 1;
			var first = new Date(selectedDate.getTime() - (selectedDate.getDay() - 1) * 86400000);
			for (i = 0; i <= 6; i++) {
				dates[i] = new Date(first.getTime() + (i * 86400000));
			}
			json.selectedDate = dates;
			
			var disDates = Common.getDataFromModel(Constants.DISABLEDDATES_MODEL);
			for (i = 0; i <= 6; i++) {
				disabledDates[i] = false;
				disabledDatesTypeName[i] = "";
				for (j = 0; j <= disDates.disabledDates.length - 1; j++) {
					if (disDates.disabledDates[j].getFullYear() === dates[i].getFullYear() && disDates.disabledDates[j].getMonth() === dates[i].getMonth() && disDates.disabledDates[j].getDate() === dates[i].getDate()) {
						disabledDates[i] = true;
						disabledDatesTypeName[i] = disDates.disabledDatesTypeName[j];
						break;
					}
				}
			}
			json.disabledDates = disabledDates;
			json.disabledDatesTypeName = disabledDatesTypeName;
			
			Common.setDataToModel(json, Constants.TIMESHEET_ENTRY_PERIOD);

			Common.setDataToModel(selectedDate, Constants.PAGE_WEEKVIEW_SELECTEDDATE);
			//this.NavigateToView("LastEntriesList");			
		//	this.NavigateToView("favoritesList");
			this.getOwnerComponent().getRouter().navTo("timeSheetSummary");
			//this.NavigateToView("projectTimePage");
		},
		startDateChangeOfCalendar: function() {
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				style: "long",
				pattern: "MMMM"
			});
			var dateFormatHeader = sap.ui.core.format.DateFormat.getDateInstance({
				style: "long",
				pattern: "MMMM YYYY"
			});

			var calendarControl = sap.ui.getCore().byId(this.createId("calendarw"));
			var startDate = calendarControl.getStartDate();
			var btnNextMonthControl = sap.ui.getCore().byId(this.createId("btnNextMonth"));
			var btnPreviousMonthControl = sap.ui.getCore().byId(this.createId("btnPreviousMonth"));

			var month = (startDate.getMonth());
			
			var minDateReal = this.getMinDateReal(new Date()); //this is real min date
			var minDate = this.getMinDate(minDateReal);

			
			jQuery(".sapUiCalHead").html(dateFormatHeader.format(startDate));
			btnNextMonthControl.setText(dateFormat.format(new Date(2017, month + 1, 1)));
			btnPreviousMonthControl.setText(dateFormat.format(new Date(2017, month - 1, 1)));

			if (startDate > minDate) {
				btnPreviousMonthControl.setEnabled(true);
			} else {
				btnPreviousMonthControl.setEnabled(false);
			}

			if (new Date(startDate.getFullYear(), month + 1, 1) <= this.getMaxDateReal(new Date())) {
				btnNextMonthControl.setEnabled(true);
			} else {
				btnNextMonthControl.setEnabled(false);
			}
			var dateFormat2 = sap.ui.core.format.DateFormat.getDateInstance({
				style: "long",
				pattern: "YYYYMMdd"
			});

			if (minDateReal !== minDate) {
				var d;
				for (d = minDate; d < minDateReal; d.setDate(d.getDate() + 1)) {
					var dd = dateFormat2.format(d);
					var sControlId = this.createId("calendarw") + "--Month0-" + dd;
					var oDay = $( "div[id$='" + sControlId +  "']" ); // sap.ui.getCore().byId(sControlId);
					if(oDay) {
						//oDay.addSyleClass("sapUiCalItemDsbl");
						oDay.removeClass("sapUiCalItemDsbl").addClass("sapUiCalItemDsbl");
					}
				}
			/*	d = this.getMinDate(minDateReal);
				for (var i = 0; i <= 10; i++) {
					d.setDate(d.getDate() - 1);
					var dd1 = dateFormat2.format(d);
					var sControlId1 = this.createId("calendarw") + "--Month0-" + dd1;
					var oDay1 = $( "div[id$='" + sControlId1 +  "']" ); // sap.ui.getCore().byId(sControlId);
					if(oDay1) {
						oDay1.removeClass("colorwhite").addClass("colorwhite");
					}
				} */
				
			}
		},
		gotoPreviousMonth: function() {
			var minDateReal = this.getMinDateReal(new Date()); //this is real min date
			var minDate = this.getMinDate(minDateReal);
			
			var calendarControl = sap.ui.getCore().byId(this.createId("calendarw"));
			var startDate = calendarControl.getStartDate();
			var month = (startDate.getMonth());
			var targetDate = new Date(startDate.getFullYear(), month - 1, 1);
			
			
			if (startDate <= minDateReal) {
				if (minDateReal !== minDate) {
					this.hideBusyIndicator();
					this.ShowPopupError(this.getResource("error13Week")); 
					return;
				}
			}

			
			if (targetDate < minDate) {
				targetDate = minDate;
			}
			calendarControl.displayDate(targetDate);
			this.startDateChangeOfCalendar();
		},
		gotoNextMonth: function() {
			var calendarControl = sap.ui.getCore().byId(this.createId("calendarw"));
			var startDate = calendarControl.getStartDate();
			var month = (startDate.getMonth());
			var targetDate = new Date(startDate.getFullYear(), month + 1, 1);
			if (targetDate > this.getMaxDate(new Date())) {
				targetDate = this.getMaxDate(new Date());
			}
			calendarControl.displayDate(targetDate);
			this.startDateChangeOfCalendar();
		},
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.deloitte.timesheet.view.weekView
		 */
		onInit: function() {
				this.setCalendarView(false);
				this.getRouter().attachRouteMatched(this.handleRouteMatched, this);
			}
			/**
			 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
			 * (NOT before the first rendering! onInit() is used for that one!).
			 * @memberOf com.deloitte.timesheet.view.weekView
			 */
			//	onBeforeRendering: function() {
			//
			//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.deloitte.timesheet.view.weekView
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.deloitte.timesheet.view.weekView
		 */
		//	onExit: function() {
		//
		//	}
	});

});