sap.ui.define(["jquery.sap.global", "sap/ui/model/json/JSONModel", 
	'sap/ui/unified/DateTypeRange',
	'sap/ui/unified/DateRange',
	'com/deloitte/timesheet/model/Constants',
	'com/deloitte/timesheet/model/Common',
"com/deloitte/timesheet/controller/BaseController"],
	function(jQuery, JSONModel,DateTypeRange, DateRange, Constants,Common,BaseController) {
		"use strict";

		return BaseController.extend("com.deloitte.timesheet.controller.favoritesList", {
			onInit: function() {
				this.getRouter().attachRouteMatched(this.handleRouteMatched, this);
			},
			handleRouteMatched: function(evt) {
				//Check whether is the detail page is matched.
				if (evt.getParameter("name") !== "favoritesList") {
					return;
				}
				this.showBusyIndicator(2000,0);
				var attendance_hours=Constants.WORK_TYPE_ATTENDANCE_HOURS_CODE;
				var status_code=Constants.WORK_TYPE_STATUS_CODE_CANCELED;
            	var lookup = {};
                var items = "";
                var distinctItemList = [];
                var favoriList = [];
                var result=[];
                var today = new Date();
                var maxDate = new Date(today.getTime()); //bugüne kadar
		    	var minDate = new Date(today.getTime() - (24 * 7 * 86400000)); //bugünden itibaren 6 ay öncesi
			   	/*var url = "TimeSheetSet(Pernr='" + Common.PersonnelNo() + "',Datefrom=" + Common.DateToString(minDate, true) + ",Dateto=" + Common.DateToString(
						maxDate, true) + ")/ActivitySet";*/
				var url = "getRecentActivitySet?Pernr='"+Common.PersonnelNo()+"'";
						
				var oODataJSONModel = Common.CallSAPMethod(url);
	        	items = oODataJSONModel.getData().results;
	        	var itemCountToShow = sap.ui.getCore().getModel().getData();
		
			    /*compare fonksiyonu sıralama yapar en son kullanılan projeyi listenin en sonuna yani sıralamada en altta
			    gözükecek şekilde sıralama yapar.
			    */
		        function compare(a,b) {
	                 if (a.Ersda < b.Ersda)
	                     return -1;
	                 if (a.Ersda > b.Ersda)
	                     return 1;
	                     return 0;
                }
				/*aşağıdaki döngüde attandance_hours a sahip ve canceled_status_code la aynı olmayan itemlar arasında distinct olanlar
				bizim item listemizdir.
				*/
				items=items.sort(compare);
				
				for(var i=(items.length-1) ; i>=0 ; i--){
					if(items[i].Awart == attendance_hours && items[i].Status != status_code){
						var concat=items[i].Vornr+"-"+items[i].Rnplnr;
						if (!(concat in lookup)) {
				    		lookup[concat] = 1;
				    		distinctItemList.push(items[i]);
						}
					}
				}
				/*
				tarihe göre sıralama yapıyoruz tarihi en küçük olandan en büyük olana doğru,
				böylece listenin en altında en son işlem yapılan proje gözükecektir.
				*/
				/*distinct listenin en sonundan başlayarak sondan maksimum 4 tanesini result listemize koyarız.*/
				for(var i=0 ; i<=(distinctItemList.length)-1 ; i++){
					if(favoriList.length<itemCountToShow){
						if(distinctItemList[i].ZzPost1!==""){
					    	distinctItemList[i].Vornr="("+distinctItemList[i].Vornr+")";
					    	distinctItemList[i].Atext="";
						}
				    	favoriList.push(distinctItemList[i]);
				    }else{
				    	break;
				    }
				}
				/*result listemizi tarihi en küçükten en büyüğe doğru sıralarız.
				*/
				//order yapılır. //en sondan en başa
				
				for(var i=(favoriList.length)-1 ; i>=0 ; i--){
			    	result.push(favoriList[i]);
			    }
			    
		    	oODataJSONModel.oData.results=result;
			    this.getView().setModel(oODataJSONModel);
			},
			
			gotoChoiceWorkingType: function() {
                Common.setDataToModel("", Constants.TIMESHEET_ENTRY_WORK_TYPE_CODE);
                Common.setDataToModel("", Constants.TIMESHEET_ENTRY_PROJECT_CODE);
                Common.setDataToModel("", Constants.TIMESHEET_ENTRY_CUSTOMER_CODE);
                Common.setDataToModel("", Constants.TIMESHEET_ENTRY_ACTIVITY_NO);
                Common.setDataToModel("", Constants.TIMESHEET_ENTRY_WORK_TYPE_NAME);
                Common.setDataToModel("", Constants.TIMESHEET_ENTRY_CUSTOMER_NAME);
                Common.setDataToModel("", Constants.TIMESHEET_ENTRY_STATUS_CODE);
                Common.setDataToModel("", Constants.TIMESHEET_ENTRY_PROJECT_NAME);
				return this.NavigateToView("searchWorkType");
			},
			
			gotoProjectTimePage: function(oEvent) {
                // The actual Item
    			var oItem = oEvent.getSource();
			    // The model that is bound to the item
    			var oContext = oItem.getBindingContext();
    		    var worktypecode=oContext.getProperty("Awart");
    		    var projectcode=$.trim(oContext.getProperty("Rnplnr"));
                // var customercode=oContext.getProperty(""); //müşteri kodu servise eklenmeli
                var activityno=oContext.getProperty("Vornr");
				if(activityno.includes("(")===true && activityno.includes(")")===true){
				    activityno=activityno.substr(1, 4);
				}
                
                var worktypename=oContext.getProperty("Atext");
                var projectname=oContext.getProperty("ZzPost1");
                var customername=oContext.getProperty("ZzCustomer");
                var statuscode=oContext.getProperty("Status");
                
                Common.setDataToModel(worktypecode, Constants.TIMESHEET_ENTRY_WORK_TYPE_CODE);
                Common.setDataToModel(projectcode, Constants.TIMESHEET_ENTRY_PROJECT_CODE);
            //  Common.setDataToModel(customercode, Constants.TIMESHEET_ENTRY_CUSTOMER_CODE); //servise eklenince gönderilecek.
                Common.setDataToModel(activityno, Constants.TIMESHEET_ENTRY_ACTIVITY_NO);
                Common.setDataToModel(worktypename, Constants.TIMESHEET_ENTRY_WORK_TYPE_NAME);
                Common.setDataToModel(customername, Constants.TIMESHEET_ENTRY_CUSTOMER_NAME);
                Common.setDataToModel(projectname, Constants.TIMESHEET_ENTRY_PROJECT_NAME);
                Common.setDataToModel(statuscode, Constants.TIMESHEET_ENTRY_STATUS_CODE);
            
    	         return this.NavigateToView("projectTimePage");
},
			backToPage: function() {
		    	return this.onNavBack();      
		    }

		});

	});