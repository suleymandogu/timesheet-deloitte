sap.ui.define([
	"sap/ui/core/mvc/Controller",
	'sap/ui/unified/DateTypeRange',
	'sap/ui/unified/DateRange',
	"com/deloitte/timesheet/controller/BaseController",
	'sap/ui/model/json/JSONModel',
	'sap/ui/model/Filter',
	'com/deloitte/timesheet/model/Constants',
	'com/deloitte/timesheet/model/Common'
], function(Controller, DateTypeRange, DateRange, BaseController, JSONModel, Filter, Constants, Common) {
	"use strict";
	var noAssignmentItem="";
    var oItem=null;
    var concatResult=[];
	return BaseController.extend("com.deloitte.timesheet.controller.searchProject", {

		onInit: function() {
			this.getRouter().attachRouteMatched(this.handleRouteMatched, this);	
		},
		handleRouteMatched: function(evt) {
				if (evt.getParameter("name") !== "searchProject") {
					return;
				}
			
				this.showBusyIndicator(3000,0);
				sap.ui.getCore().byId(this.createId("searchField")).setValue(""); //clear search history
				oItem=null;
			   	var items = "";
				noAssignmentItem="";
                var serviceResult = [];
                concatResult=[];
                var lookup = {};
                var attendance_hours=Constants.WORK_TYPE_ATTENDANCE_HOURS_CODE;
				var status_code=Constants.WORK_TYPE_STATUS_CODE_CANCELED;
			    var url = "getRecentActivitySet?Pernr='"+Common.PersonnelNo()+"'";
						
				var oODataJSONModel = Common.CallSAPMethod(url);
	        	items = oODataJSONModel.getData().results;	
         
         		///sıralama yakın tarih en üstde uzak tarih en sona doğru
	        	function compare(a,b) {
	                 if (a.Ersda > b.Ersda)
	                     return -1;
	                 if (a.Ersda < b.Ersda)
	                     return 1;
					return 0;
				}
				
				items=items.sort(compare);
				
        		for(var i=0 ; i<=(items.length)-1 ; i++){
				if(items[i].Awart === attendance_hours && items[i].Status !== status_code){ //attendance olanlar ve cancel yapılmayanlar
					var concat=items[i].Vornr+"-"+items[i].Rnplnr;
					if (!(concat in lookup)) {
			    		if(items[i].ZzPost1!=="" && items[i].ZzPost1!=="No Assignment ID" ){ //Proje adı boş ve ya No Assignment Id değilse
					    		lookup[concat] = 1;
					    		serviceResult.push(items[i]);
			    			}
			    		if(items[i].ZzPost1==="No Assignment ID"){
			    			noAssignmentItem = items[i];
			    		}	
						}
					}
				}
			   //no assignment id listenin en başına eklenir. 
			   if(noAssignmentItem!==null && noAssignmentItem!==""){
			     concatResult.push(noAssignmentItem);
			   }
			
			   //tarih sıralaması en yakın olandan en uzak olana doğru listeye ekleme yapılır.
				for(var i=0 ; i<=(serviceResult.length)-1 ; i++){
					concatResult.push(serviceResult[i]);
				}
				
				oODataJSONModel.oData.results=concatResult;
				this.getView().setModel(oODataJSONModel);
				
			},
		
		
		ShowSearchedData: function() {
			var oData = Common.getDataFromModel(Constants.ASYNCHRONOUS_DATA_RETURND);
			var me = Common.getDataFromModel(Constants.CURRENT_CONTROLLER);
			var oODataJSONModel = new sap.ui.model.json.JSONModel();
			oODataJSONModel.setData(oData);
			var busycontrol = sap.ui.getCore().byId(me.createId("isBusy"));
			busycontrol.setVisible(false);
			me.getView().setModel(oODataJSONModel);
			return oODataJSONModel;
		},

		onSearch: function(oEvt) {

			var oData = null;
			var oODataJSONModel = new sap.ui.model.json.JSONModel();
			
			var text=sap.ui.getCore().byId(this.createId("searchField")).getValue(); 
			if(text===""){ //komple temizlenirse search, son kullandığı projeleri listeliyoruz tekrar sayfanın ilk açılan haline geri dönüyor
				oODataJSONModel.oData.results=concatResult;
				this.getView().setModel(oODataJSONModel);
			}else{
			
			oODataJSONModel.setData(oData);
			this.getView().setModel(oODataJSONModel);

			var sQuery = oEvt.getSource().getValue();
			if (sQuery && sQuery.length > 2) {
				var busycontrol = sap.ui.getCore().byId(this.createId("isBusy"));
				busycontrol.setVisible(true);
				var url = "/AssignmentSet?$skip=0&top=100&$filter=substringof('" + encodeURIComponent(sQuery) + "',SoldToNameC) or substringof('" +
					encodeURIComponent(sQuery) +
					"',Vbeln) or substringof('" + encodeURIComponent(sQuery) + "',BstkdEC)"+"&$orderby=Vbeln desc";
				Common.setDataToModel(this, Constants.CURRENT_CONTROLLER);
				Common.CallSAPMethodasynch(url, this.ShowSearchedData);
			}
			}
		},
		selectProject: function(oEvt) {
			    oItem = oEvt.getSource();
				// The model that is bound to the item
				var oContext = oItem.getBindingContext();
				
				var customername = oContext.getProperty("SoldToNameC");
				var projectname = oContext.getProperty("BstkdEC");
				var projectcode = oContext.getProperty("Vbeln");
				
				var customername_ex = oContext.getProperty("ZzCustomer");
				var projectname_ex = oContext.getProperty("ZzPost1");
				var projectcode_ex = oContext.getProperty("Rnplnr");

				var customercode = oContext.getProperty("SoldTo");
				
				if(customername!==null && customername!=="" && customername!==undefined){

				Common.setDataToModel(projectcode, Constants.TIMESHEET_ENTRY_PROJECT_CODE);
				Common.setDataToModel(projectname, Constants.TIMESHEET_ENTRY_PROJECT_NAME);

				Common.setDataToModel(customercode, Constants.TIMESHEET_ENTRY_CUSTOMER_CODE); //servise eklenince gönderilecek.
				Common.setDataToModel(customername, Constants.TIMESHEET_ENTRY_CUSTOMER_NAME);
				}else{
				Common.setDataToModel(projectcode_ex, Constants.TIMESHEET_ENTRY_PROJECT_CODE);
				Common.setDataToModel(projectname_ex, Constants.TIMESHEET_ENTRY_PROJECT_NAME);
				Common.setDataToModel(customername_ex, Constants.TIMESHEET_ENTRY_CUSTOMER_NAME);
				}
				if(projectname_ex==="No Assignment ID"){
					this.NavigateToView("projectTimePage");
				}else{
					this.NavigateToView("searchActivityType");
				}
			},
			 
	    onPressCancel:function(){
			return this.NavigateToView("timeSheetSummary");
		}
		
	});

});