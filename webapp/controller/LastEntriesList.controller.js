sap.ui.define(["jquery.sap.global", 
        "sap/ui/model/json/JSONModel",
		"com/deloitte/timesheet/controller/BaseController",
		"com/deloitte/timesheet/model/Constants",
		"com/deloitte/timesheet/model/Common"
	],
	function (jQuery, JSONModel, BaseController, Constants, Common) {
		"use strict";

		return BaseController.extend("com.deloitte.timesheet.controller.LastEntriesList", {

			/**
			 * Called when a controller is instantiated and its View controls (if available) are already created.
			 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
			 * @memberOf com.deloitte.timesheet.view.LastEntriesList
			 */

			onInit: function () {

				//this._oRouter = this.getRouter();
				this.getRouter().attachRouteMatched(this.handleRouteMatched, this);

			},
			handleRouteMatched: function (evt) {
				//Check whether is the detail page is matched.
				if (evt.getParameter("name") !== "LastEntriesList") {
					return;
				}
				//You code here to run every time when your detail page is called.
				this.showBusyIndicator(2000, 0);
				var oODataJSONModel = null;
				var threeWeeksAgo = new Date();
				threeWeeksAgo.setDate(threeWeeksAgo.getDate() - 21);

				// // set explored app's demo model on this sample
				// var oModel = new JSONModel(jQuery.sap.getModulePath("sap.ui.demo.mock", "/products.json"));
				// this.getView().setModel(oModel);

				var sServiceUrl = "/sap/opu/odata/sap/ZTR_HR_ESS01_SRV/";
				// Create and set domain model to the component
				var oModel = new sap.ui.model.odata.ODataModel(sServiceUrl, {
					json: true,
					loadMetadataAsync: true
				});

				// insert url parameters
				oModel.read("getPersonWorkList?InputDate=datetime'2016-11-16T00%3A00%3A00'&Pernr=%27%27",
					null,
					null,
					false,
					function (oData, oResponse) {
						// create JSON model
						oODataJSONModel = new sap.ui.model.json.JSONModel();

						// set the odata JSON as data of JSON model
						oODataJSONModel.setData(oData);
						//oODataJSONModel = oData;
					});
				this.getView().setModel(oODataJSONModel);

				var sselectedWeek = Common.getDataFromModel(Constants.TIMESHEET_ENTRY_PERIOD);
				//sap.ui.getCore().getModel("SelectedWeek");

				var i;
				var s = "";
				var dates = sselectedWeek.selectedDate;
				//var dates = sselectedWeek.getData().selectedDate;
				for (i = 0; i <= 6; i++) {
					s = s + dates[i] + "\r\n";
				}
				sap.ui.getCore().byId(this.createId("text1")).setText(s);
			},
			gotoPage: function () {
				return this.NavigateToView("weekView");
			}

			/**
			 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
			 * (NOT before the first rendering! onInit() is used for that one!).
			 * @memberOf com.deloitte.timesheet.view.LastEntriesList
			 */
			//onBeforeRendering: function() {

			//}

			/**
			 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
			 * This hook is the same one that SAPUI5 controls get after being rendered.
			 * @memberOf com.deloitte.timesheet.view.LastEntriesList
			 */
			//	onAfterRendering: function() {
			//
			//	},

			/**
			 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
			 * @memberOf com.deloitte.timesheet.view.LastEntriesList
			 */
			//	onExit: function() {
			//
			//	}

		});

	});