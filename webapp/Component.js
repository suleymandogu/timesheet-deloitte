sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"com/deloitte/timesheet/model/models",
	"com/deloitte/timesheet/model/Common"
], function(UIComponent, Device, models, Common) {
	"use strict";
	var numberOfLastUsedItems = 4;
	var mq1;
	var mq2;
			
	return UIComponent.extend("com.deloitte.timesheet.Component", {

		metadata: {
			manifest: "json" 
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
	
			Common.LoadPersonnelInfoSAP();
			
			//application rootpath (for images specially) 	
			var oRootPath = jQuery.sap.getModulePath("com.deloitte.timesheet"); // your resource root
			var oImageModel = new sap.ui.model.json.JSONModel({
				path : oRootPath
			}); 
			this.setModel(oImageModel, "rootPathModel");	
			
			
			
			this.getRouter().initialize();
	
		   	if (matchMedia) {
				  mq1 = window.matchMedia("(min-width: 0px) and (max-width: 326px) and (orientation: portrait)");
				  mq2 = window.matchMedia("(min-width: 0px) and (max-width: 580px) and (orientation: landscape)");
				  mq1.addListener(this.WidthChange);
				  mq2.addListener(this.WidthChange);
				  this.WidthChange(mq1);
		   	}
		
		},
			// media query change
			WidthChange: function (mq) {
				if (mq1.matches || mq2. matches) { // window is iphone5 or smaller
					numberOfLastUsedItems	= 4 ;
				} else { // window is iphone6 or larger
					numberOfLastUsedItems	= 6 ;
				}
				var oModel = new sap.ui.model.json.JSONModel();
				oModel.setData(numberOfLastUsedItems);
				sap.ui.getCore().setModel(oModel);
			}
	});
});