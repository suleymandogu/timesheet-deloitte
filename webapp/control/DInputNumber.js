sap.ui.define(
  ['com/deloitte/timesheet/control/DInputText'],
  function(Control) {
  return Control.extend("com.deloitte.timesheet.control.DInputNumber",{
    init: function () {
        this.setType("Number");
        this.setPattern("[0-9]*");
        Control.prototype.init.call(this);
    },
    renderer: {}
  });
  }
);