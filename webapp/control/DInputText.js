sap.ui.define(
  ['sap/m/Input'],
  function(Control) {
  return Control.extend("com.deloitte.timesheet.control.DInputText",{
      metadata: {
        properties: {
            pattern: "string"
        }
    },
    renderer: {}
  });
  }
);

com.deloitte.timesheet.control.DInputTextRenderer.writeInnerAttributes = function (oRm, oControl) {
    sap.m.InputRenderer.writeInnerAttributes.apply(this, arguments);
    oRm.writeAttributeEscaped("pattern", oControl.getPattern());
};