sap.ui.define([
	'sap/m/Button',
	'sap/m/Dialog',
	'sap/m/Label',
	'sap/m/Text',
	'sap/m/TextArea',
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	'sap/m/MessageBox', 'sap/ui/core/BusyIndicator', 'jquery.sap.global'
], function (Button, Dialog, Label, Text, TextArea, Controller, History, MessageBox, BusyIndicator, jQuery) {
	"use strict";

	return Controller.extend("com.deloitte.timesheet.controller.BaseController", {
		/**
		 * Convenience method for accessing the router.
		 * @public
		 * @returns {sap.ui.core.routing.Router} the router for this component
		 */
		getRouter: function () {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		/**
		 * Convenience method for getting the view model by name.
		 * @public
		 * @param {string} [sName] the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		getModel: function (sName) {
			return this.getView().getModel(sName);
		},

		/**
		 * Convenience method for setting the view model.
		 * @public
		 * @param {sap.ui.model.Model} oModel the model instance
		 * @param {string} sName the model name
		 * @returns {sap.ui.mvc.View} the view instance
		 */
		setModel: function (oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},

		/**
		 * Getter for the resource bundle.
		 * @public
		 * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
		 */
		getResourceBundle: function () {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

		NavigateToView: function (viewName) {
			var router = this.getRouter();
			return router.navTo(viewName);
			//var view = sap.ui.getCore().byId("viewWeek"); //sap.ui.view({id:"LastEntriesList", viewName:"LastEntriesList", type:sap.ui.core.mvc.ViewType.XML});
			//oShell.setContent(view); // or oPanel.setContent() 	
		},
		onNavBack: function () {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				window.history.go(-1);

			} else {
				//var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				//oRouter.navTo("overview", true);
			}
		},
		
		ShowPopupError: function (sMessage) {
			var dialog = new Dialog({
				title: this.getResource("warning"),
				type: 'Message',
				content: new Text({
					text: sMessage
				}),
				beginButton: new Button({
					text: this.getResource("ok"),
					press: function () {
						dialog.close();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},

		ShowPopupErrorEx: function (sMessage) {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.warning(
				sMessage, {
					styleClass: bCompact ? "sapUiSizeCompact alignCenter" : "alignCenter"
				}
			);
		},

		ShowPopupSuccess: function (sMessage) {
			var dialog = new Dialog({
				title: this.getResource("success"),
				type: 'Message',
				content: new Text({
					text: sMessage
				}),
				beginButton: new Button({
					text: this.getResource("ok"),
					press: function () {
						dialog.close();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},

		ShowPopupSuccessEx: function (sMessage) {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.success(
				sMessage, {
					styleClass: bCompact ? "sapUiSizeCompact alignCenter" : "alignCenter"
				}
			);
		},

		ShowPopupConfirm: function (sMessage) {
			var dialog = new Dialog({
				title: this.getResource("confirm"),
				type: 'Message',
				content: new Text({
					text: sMessage
				}),
				beginButton: new Button({
					text: this.getResource("ok"),
					press: function () {
						dialog.close();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},

		ShowPopupConfirmEx: function (sMessage) {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.confirm(
				sMessage, {
					styleClass: bCompact ? "sapUiSizeCompact alignCenter" : "alignCenter"
				}
			);
		},

		getResource: function (sResourceKey) {
			var oRootPath = jQuery.sap.getModulePath("com.deloitte.timesheet"); // your resource root
			jQuery.sap.require("jquery.sap.resources");
			var sLocale = sap.ui.getCore().getConfiguration().getLanguage();
			var oBundle = jQuery.sap.resources({
				url: oRootPath + "/i18n/i18n.properties",
				locale: sLocale
			});
			return oBundle.getText(sResourceKey);
		},
		
		hideBusyIndicator: function () {
			sap.ui.core.BusyIndicator.hide();
		},
		
		showBusyIndicator: function (iDuration, iDelay) {
			sap.ui.core.BusyIndicator.show(iDelay);
			var oDay = $(".sapUiLocalBusyIndicator");
			oDay.addClass("sapUiLocalBusyIndicatorFade");
			if (iDuration && iDuration > 0) {
				if (this._sTimeoutId) {
					jQuery.sap.clearDelayedCall(this._sTimeoutId);
					this._sTimeoutId = null;
				}
				this._sTimeoutId = jQuery.sap.delayedCall(iDuration, this, function () {
					this.hideBusyIndicator();
				});
			}
		},

		controlCancel: function (page) {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			if (oHistory.aHistory[oHistory.aHistory.length - 2] !== page) {
				return false;
			} else {
				return true;
			}
		}
	});
});