sap.ui.define([
	'sap/m/Button',
	'sap/m/Dialog',
	'sap/m/Label',
	'sap/m/Text',
	'sap/m/TextArea',
	'jquery.sap.global',
	"sap/ui/core/mvc/Controller",
	'sap/m/MessageBox',
	'sap/ui/unified/CalendarLegendItem',
	'sap/ui/unified/DateTypeRange',
	'sap/ui/unified/DateRange',
	"com/deloitte/timesheet/controller/BaseController",
	'sap/ui/model/json/JSONModel',
	'com/deloitte/timesheet/model/Constants',
	'com/deloitte/timesheet/model/Common',
	'sap/ui/core/Fragment',
	'sap/m/MessageToast',
	'sap/ui/core/BusyIndicator',
	'jquery.sap.script'
], function (Button, Dialog, Label, Text, TextArea, jQuery, Controller, MessageBox, CalendarLegendItem, DateTypeRange, DateRange,
	BaseController, JSONModel, Constants, Common, Fragment,
	MessageToast, BusyIndicator, jQuerySapScript) {
	"use strict";
	var gCounter = null;
	var gProjectCode = null;
	var gWorktype = null;
	var ISOworkDate = null;
	var onlyWorkDate = null;
	var mySelf = null;
	var lastValue = null;
	var activityNo = "";
	var textFrmt = "";
	return BaseController.extend("com.deloitte.timesheet.controller.timeSheetSummary", {
		handleRouteMatched: function (evt) {
			if (evt.getParameter("name") !== "timeSheetSummary") {
				return;
			}
			this.loadData();
		},
		deleteItem: function (evt) {
			var errorMessage = "";
			var textError = "";
			var url = "ActivitySet(Pernr='" + Common.PersonnelNo() + "',Workdate=" + ISOworkDate + ",Counter='" + gCounter + "')"; //delete için url	
			if (this.getResource("lang") === "tr") {
				textFrmt = onlyWorkDate + " " + this.getResource("deleteConfirm") + "?";
			} else {
				textFrmt = this.getResource("deleteConfirm") + " " + onlyWorkDate + "?";
			}
			var oList = evt.getSource().getParent();
			var dialog = new Dialog({
				title: this.getResource("deleteHour"),
				type: 'Message',
				content: new Text({
					text: textFrmt
				}),

				beginButton: new Button({
					text: this.getResource("delete"),
					press: function () {
						try {
							Common.DeleteItem(url);
							MessageToast.show(mySelf.getResource("deleteSuccesful"), {
								animationDuration: "3000",
								width: "100%",
								at: {
									LeftTop: 0
								}
							});
							oList.removeAggregation("items", oList.getSwipedItem());
							oList.swipeOut();
						} catch (err) {
							errorMessage = err.response.body.split(':')[5].split('}')[0]; //err.response.body.message.value değerini alıyoruz sadece
							if (errorMessage !== "") {
								textError = mySelf.getResource("registrationError") + " " + errorMessage + " " + mySelf.getResource("connectIT");
								mySelf.ShowPopupError(textError);
							}
						}
						dialog.close();
					}
				}),
				endButton: new Button({
					/*ender abi bakacak!!*/
					text: this.getResource("back"),
					press: function () {
						evt.getSource().setVisible(false);
						evt.preventDefault();
						dialog.close();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
			gCounter = null;
			gWorktype = null;
			gProjectCode = null;
			ISOworkDate = null;
			onlyWorkDate = null;
			textFrmt = "";
		},
		handleSwipe: function (e) { // register swipe event
			var oSwipeListItem = e.getParameter("listItem"); // get swiped content from event
			var oContext = oSwipeListItem.getBindingContext();
			gCounter = oContext.getProperty("Counter");
			gWorktype = oContext.getProperty("Atext");
			gProjectCode = oContext.getProperty("Rnplnr");
			ISOworkDate = Common.DateToString(oContext.getProperty("Workdate"), true);
			onlyWorkDate = Common.OnlyDateShow(oContext.getProperty("Workdate"));
		},
		compare: function (a, b) {
			if (a.Rnplnr < b.Rnplnr)
				return -1;
			if (a.Rnplnr > b.Rnplnr)
				return 1;
			return 0;
		},

		loadData: function () {
			var sselectedWeek = Common.getDataFromModel(Constants.TIMESHEET_ENTRY_PERIOD);
			var i;
			var s = "";
			var result = [];
			mySelf = this;
			activityNo = "";
			if (!sselectedWeek) { //tarih seçilmeden bu ekrana girilemez.
				return;
			}
			var dates = sselectedWeek.selectedDate;
			var disabledDates = sselectedWeek.disabledDates;
			var disabledDatesTypeName = sselectedWeek.disabledDatesTypeName;

			for (i = 0; i <= 6; i++) {
				s = s + dates[i] + "\r\n";
			}
			var oPage = sap.ui.getCore().byId(this.createId("timeSheetSummary"));
			var first = dates[0];
			var last = dates[5];
			oPage.setTitle(Common.OnlyDateShow(first) + " - " + Common.OnlyDateShow(last));

			var url = "TimeSheetSet(Pernr='" + Common.PersonnelNo() + "',Datefrom=" + Common.DateToString(first, true) + ",Dateto=" + Common.DateToString(
				last, true) + ")/ActivitySet";
			var oODataJSONModel = Common.CallSAPMethod(url);
			var datum = oODataJSONModel.getData().results;

			//var mondayList = null;
			for (var i = 0; i < datum.length; i++) {
				if (datum[i].Status !== Constants.WORK_TYPE_STATUS_CODE_CANCELED && parseFloat(datum[i].Catshours) !== 0 && datum[i].Catshours !==
					null && datum[i].Catshours !== "" && datum[i].Status !== Constants.WORK_TYPE_STATUS_CODE_CHANGED_AFTER_APPROVAL)
					result.push(datum[i]);
			}
			result.sort(this.compare);
			oODataJSONModel.oData.results = result;
			this.getView().setModel(oODataJSONModel);

			var allData = new sap.ui.model.json.JSONModel();
			var json = {};
			var mondayData = [];
			var tuesdayData = [];
			var wednesdayData = [];
			var thursdayData = [];
			var fridayData = [];
			var saturdayData = [];
			var alwaysTrue = true;
			json.mondayCount = 0;
			json.tuesdayCount = 0;
			json.wednesdayCount = 0;
			json.thursdayCount = 0;
			json.fridayCount = 0;
			json.saturdayCount = 0;
			if (alwaysTrue) { //if(datum.length !== 0){	
				for (var i = 0; i < result.length; i++) {
					if (result[i].Workdate.getFullYear() === dates[0].getFullYear() && result[i].Workdate.getMonth() === dates[0].getMonth() &&
						result[i].Workdate.getDay() === dates[0].getDay()) {
						result[i] = this.setLabels(result, i);
						mondayData.push(result[i]);
						if (result[i].Vornr !== null && result[i].Vornr !== "" && result[i].Vornr !== undefined) {
							result[i].Vornr = "(" + result[i].Vornr + ")";
						}
						json.mondayCount += parseFloat(result[i].Catshours);
					}
					if (result[i].Workdate.getFullYear() === dates[1].getFullYear() && result[i].Workdate.getMonth() === dates[1].getMonth() &&
						result[i].Workdate.getDay() === dates[1].getDay()) {
						result[i] = this.setLabels(result, i);
						tuesdayData.push(result[i]);
						if (result[i].Vornr !== null && result[i].Vornr !== "" && result[i].Vornr !== undefined) {
							result[i].Vornr = "(" + result[i].Vornr + ")";
						}
						json.tuesdayCount += parseFloat(result[i].Catshours);
					}
					if (result[i].Workdate.getFullYear() === dates[2].getFullYear() && result[i].Workdate.getMonth() === dates[2].getMonth() &&
						result[i].Workdate.getDay() === dates[2].getDay()) {
						result[i] = this.setLabels(result, i);
						wednesdayData.push(result[i]);
						if (result[i].Vornr !== null && result[i].Vornr !== "" && result[i].Vornr !== undefined) {
							result[i].Vornr = "(" + result[i].Vornr + ")";
						}
						json.wednesdayCount += parseFloat(result[i].Catshours);
					}
					if (result[i].Workdate.getFullYear() === dates[3].getFullYear() && result[i].Workdate.getMonth() === dates[3].getMonth() &&
						result[i].Workdate.getDay() === dates[3].getDay()) {
						result[i] = this.setLabels(result, i);
						thursdayData.push(result[i]);
						if (result[i].Vornr !== null && result[i].Vornr !== "" && result[i].Vornr !== undefined) {
							result[i].Vornr = "(" + result[i].Vornr + ")";
						}
						json.thursdayCount += parseFloat(result[i].Catshours);
					}
					if (result[i].Workdate.getFullYear() === dates[4].getFullYear() && result[i].Workdate.getMonth() === dates[4].getMonth() &&
						result[i].Workdate.getDay() === dates[4].getDay()) {
						result[i] = this.setLabels(result, i);
						fridayData.push(result[i]);
						if (result[i].Vornr !== null && result[i].Vornr !== "" && result[i].Vornr !== undefined) {
							result[i].Vornr = "(" + result[i].Vornr + ")";
						}
						json.fridayCount += parseFloat(result[i].Catshours);
					}
					if (result[i].Workdate.getFullYear() === dates[5].getFullYear() && result[i].Workdate.getMonth() === dates[5].getMonth() &&
						result[i].Workdate.getDay() === dates[5].getDay()) {
						result[i] = this.setLabels(result, i);
						saturdayData.push(result[i]);
						if (result[i].Vornr !== null && result[i].Vornr !== "" && result[i].Vornr !== undefined) {
							result[i].Vornr = "(" + result[i].Vornr + ")";
						}
						json.saturdayCount += parseFloat(result[i].Catshours);
					}

				}
				//btnChanges
				if (mondayData.length === 0 && tuesdayData.length === 0 && wednesdayData.length === 0 && thursdayData.length === 0 && fridayData.length ===
					0 && saturdayData.length === 0) {
					sap.ui.getCore().byId(this.createId("btnNewtimeSheetXYZ")).setVisible(false);
					if (this.getResource("lang") === "tr") {
						sap.ui.getCore().byId(this.createId("btnWorkHourAddTr")).setVisible(true);
						sap.ui.getCore().byId(this.createId("btnWorkHourAddEng")).setVisible(false);
						sap.ui.getCore().byId(this.createId("btnWorkHourAdd")).setVisible(false);
					} else if (this.getResource("lang") === "eng") {
						sap.ui.getCore().byId(this.createId("btnWorkHourAddTr")).setVisible(false);
						sap.ui.getCore().byId(this.createId("btnWorkHourAddEng")).setVisible(true);
						sap.ui.getCore().byId(this.createId("btnWorkHourAdd")).setVisible(false);
					} else {
						sap.ui.getCore().byId(this.createId("btnWorkHourAddTr")).setVisible(false);
						sap.ui.getCore().byId(this.createId("btnWorkHourAddEng")).setVisible(false);
						sap.ui.getCore().byId(this.createId("btnWorkHourAdd")).setVisible(true);
					}
				} else {
					sap.ui.getCore().byId(this.createId("btnNewtimeSheetXYZ")).setVisible(true);
					sap.ui.getCore().byId(this.createId("btnWorkHourAddTr")).setVisible(false);
					sap.ui.getCore().byId(this.createId("btnWorkHourAddEng")).setVisible(false);
					sap.ui.getCore().byId(this.createId("btnWorkHourAdd")).setVisible(false);
				}

				//MONDAY
				json.monday = mondayData;
				json.mondayTitle = this.getResource("monday") + " (" + Common.OnlyDayMonthShow(dates[0]) + ") - " + this.getResource("total") +
					" " + json.mondayCount + " " + this.getResource("hourTitle");
				if (json.mondayCount < 8) {
					json.mondayOval = "{rootPathModel>/path}/image/redOval.svg";
				} else {
					json.mondayOval = "{rootPathModel>/path}/image/greenOval.svg";
				}
				//TUESDAY
				json.tuesday = tuesdayData;
				json.tuesdayTitle = this.getResource("tuesday") + " (" + Common.OnlyDayMonthShow(dates[1]) + ") - " + this.getResource("total") +
					" " + json.tuesdayCount + " " + this.getResource("hourTitle");
				if (json.tuesdayCount < 8) {
					json.tuesdayOval = "{rootPathModel>/path}/image/redOval.svg";
				} else {
					json.tuesdayOval = "{rootPathModel>/path}/image/greenOval.svg";
				}
				//WEDNESDAY
				json.wednesday = wednesdayData;
				json.wednesdayTitle = this.getResource("wednesday") + " (" + Common.OnlyDayMonthShow(dates[2]) + ") - " + this.getResource("total") +
					" " + json.wednesdayCount + " " + this.getResource("hourTitle");
				if (json.wednesdayCount < 8) {
					json.wednesdayOval = "{rootPathModel>/path}/image/redOval.svg";
				} else {
					json.wednesdayOval = "{rootPathModel>/path}/image/greenOval.svg";
				}
				//THURSDAY
				json.thursday = thursdayData;
				json.thursdayTitle = this.getResource("thursday") + " (" + Common.OnlyDayMonthShow(dates[3]) + ") - " + this.getResource("total") +
					" " + json.thursdayCount + " " + this.getResource("hourTitle");
				if (json.thursdayCount < 8) {
					json.thursdayOval = "{rootPathModel>/path}/image/redOval.svg";
				} else {
					json.thursdayOval = "{rootPathModel>/path}/image/greenOval.svg";
				}
				//FRIDAY
				json.friday = fridayData;
				json.fridayTitle = this.getResource("friday") + " (" + Common.OnlyDayMonthShow(dates[4]) + ") - " + this.getResource("total") +
					" " + json.fridayCount + " " + this.getResource("hourTitle");
				if (json.fridayCount < 8) {
					json.fridayOval = "{rootPathModel>/path}/image/redOval.svg";
				} else {
					json.fridayOval = "{rootPathModel>/path}/image/greenOval.svg";
				}
				//SATURDAY
				json.saturday = saturdayData;
				json.saturdayTitle = this.getResource("saturday") + " (" + Common.OnlyDayMonthShow(dates[5]) + ") - " + this.getResource("total") +
					" " + json.saturdayCount + " " + this.getResource("hourTitle");
				if (json.saturdayCount < 5) {
					json.saturdayOval = "{rootPathModel>/path}/image/redOval.svg";
				} else {
					json.saturdayOval = "{rootPathModel>/path}/image/greenOval.svg";
				}

				//TATİL GÜNLERİ ya da GRİRİŞ YAPILAMAMIŞ GÜNLER 
				for (var i = 0; i < dates.length; i++) {
					if (disabledDates[i] === true) {
						if (i === 0) {
							json.mondayNoDataTextForTimeSheetsummary = disabledDatesTypeName[i];
						}
						if (i === 1) {
							json.tuesdayNoDataTextForTimeSheetsummary = disabledDatesTypeName[i];
						}
						if (i === 2) {
							json.wednesdayNoDataTextForTimeSheetsummary = disabledDatesTypeName[i];
						}
						if (i === 3) {
							json.thursdayNoDataTextForTimeSheetsummary = disabledDatesTypeName[i];
						}
						if (i === 4) {
							json.fridayNoDataTextForTimeSheetsummary = disabledDatesTypeName[i];
						}
						if (i === 5) {
							json.saturdayNoDataTextForTimeSheetsummary = disabledDatesTypeName[i];
						}

					} else {
						if (i === 0) {
							json.mondayNoDataTextForTimeSheetsummary = this.getResource("NoDataTextForTimeSheetsummary");
						}
						if (i === 1) {
							json.tuesdayNoDataTextForTimeSheetsummary = this.getResource("NoDataTextForTimeSheetsummary");
						}
						if (i === 2) {
							json.wednesdayNoDataTextForTimeSheetsummary = this.getResource("NoDataTextForTimeSheetsummary");
						}
						if (i === 3) {
							json.thursdayNoDataTextForTimeSheetsummary = this.getResource("NoDataTextForTimeSheetsummary");
						}
						if (i === 4) {
							json.fridayNoDataTextForTimeSheetsummary = this.getResource("NoDataTextForTimeSheetsummary");
						}
						if (i === 5) {
							json.saturdayNoDataTextForTimeSheetsummary = this.getResource("NoDataTextForTimeSheetsummary");
						}
					}
				}

				allData.setData(json);
				this.getView().setModel(allData);
			}
		},

		setLabels: function (result, i) {
			if (result[i].Awart === Constants.WORK_TYPE_ATTENDANCE_HOURS_CODE) {
				if (result[i].ZzPost1 !== "") {
					result[i].Atext = "";
				}
			} else {
				result[i].ZzCustomer = "";
				result[i].ZzPost1 = "";
				result[i].Rnplnr = "";
			}
			result[i].Catshours = parseFloat(result[i].Catshours); //8,00 ı 8 olarak 1,50 yi 1,5 olarak göstermek için
			return result[i];
		},

		onInit: function () {
			this.getRouter().attachRouteMatched(this.handleRouteMatched, this);
		},

		weekSelect: function () {
			var json = {};
			var i = 0;
			var dates = [];
			var disabledDates = [];
			var disabledDatesTypeName = [];
			var selectedDate = new Date(2016, 10, 23);

			var first = new Date(selectedDate.getTime() - (selectedDate.getDay() - 1) * 86400000);
			for (i = 0; i <= 6; i++) {
				dates[i] = new Date(first.getTime() + (i * 86400000));
			}
			json.selectedDate = dates;

			for (i = 0; i <= 6; i++) {
				disabledDates[i] = false;
				disabledDatesTypeName[i] = "";
			}
			json.disabledDates = disabledDates;
			json.disabledDatesTypeName = disabledDatesTypeName;

			Common.setDataToModel(json, Constants.TIMESHEET_ENTRY_PERIOD);
		},

		getValue: function (evt) {
			var oItem = evt.getSource();
			lastValue = oItem._lastValue;
		},
		changeValue: function (evt) {
			//  var oInput = sap.ui.getCore().byId(this.createId(input));
			var errorMessage = "";
			var textError = "";

			this.showBusyIndicator(3000, 0);
			var checkLoad = 0;
			var oItem = evt.getSource();
			// The model that is bound to the item
			var oContext = oItem.getBindingContext();
			var oEntry = {};

			oEntry = Common.CreateEntry(Common.PersonnelNo(), Common.DateToISODateTime(oContext.getProperty("Workdate")), oContext.getProperty(
					"Catshours"),
				"update", oContext.getProperty("Rnplnr"), oContext.getProperty("Vornr"), oContext.getProperty("Awart"));
			oEntry.Counter = oContext.getProperty("Counter");

			var totalHours = evt.getSource().getParent().getParent().getParent().getCount();
			var checkHours = parseFloat(totalHours) - parseFloat(lastValue) + parseFloat(oEntry.Catshours);
			if (checkHours > 11) {
				this.hideBusyIndicator();
				this.ShowPopupError(this.getResource("remainingHour") + " " + (11 - parseFloat(totalHours)) + " " + this.getResource("hourEnter"));
				mySelf.loadData();
				checkLoad = 1;
				return;
			}

			var url = "ActivitySet(Pernr='" + Common.PersonnelNo() + "',Workdate=" + Common.DateToString(oContext.getProperty("Workdate"), true) +
				",Counter='" + oEntry.Counter + "')"; //update içinvar url = 	
			// Create and set domain model to the component
			try {
				var oModelY = Common.SetDomainModelComponent();
				// insert url parameters
				oModelY.update(url, oEntry, {
					method: "PUT",
					success: function () {
						mySelf.loadData();
					},
					error: function (err) {
						throw err;
					}
				});
				MessageToast.show(mySelf.getResource("operationSuccessful"), {
					animationDuration: "3000",
					width: "100%",
					at: {
						LeftTop: 0
					}
				});
			} catch (err) {
				oItem.setValue(lastValue); //değiştirilmeden önceki değer set edilir
				errorMessage = err.response.body.split(':')[5].split('}')[0]; //err.response.body.message.value değerini alıyoruz sadece
				if (errorMessage !== "") {
					textError = mySelf.getResource("registrationError") + " " + errorMessage + " " + mySelf.getResource("connectIT");
					mySelf.ShowPopupError(textError);
				}
			}
		},

		gotoPage: function () {
			this.showBusyIndicator(2000, 0);
			this.NavigateToView("favoritesList");
		},

		backToPage: function () {
			return this.NavigateToView("weekView");
		},

		test: function (oEvent) {
			var oItem = oEvent.getSource();
			var oStyle = $(oItem.getId()); // sap.ui.getCore().byId(sControlId);

			oStyle.addClass("changeBackGroud");
		},

		gotoProjectTimePage: function (oEvent) {
			// The actual Item
			// this.showBusyIndicator(2000, 0);
			var oItem = oEvent.getSource();
			var oStyle = $(oItem.getId()); // sap.ui.getCore().byId(sControlId);

			oStyle.addClass("changeBackGroud");
			// The model that is bound to the item
			var oContext = oItem.getBindingContext();
			var worktypecode = oContext.getProperty("Awart");
			var projectcode = $.trim(oContext.getProperty("Rnplnr"));
			// var customercode=oContext.getProperty(""); //müşteri kodu servise eklenmeli
			var activityno = oContext.getProperty("Vornr");
			var worktypename = oContext.getProperty("Atext");
			var projectname = oContext.getProperty("ZzPost1");
			var customername = oContext.getProperty("ZzCustomer");
			var statuscode = oContext.getProperty("Status");

			Common.setDataToModel(worktypecode, Constants.TIMESHEET_ENTRY_WORK_TYPE_CODE);
			Common.setDataToModel(projectcode, Constants.TIMESHEET_ENTRY_PROJECT_CODE);
			//  Common.setDataToModel(customercode, Constants.TIMESHEET_ENTRY_CUSTOMER_CODE); //servise eklenince gönderilecek.
			Common.setDataToModel(activityno, Constants.TIMESHEET_ENTRY_ACTIVITY_NO);
			Common.setDataToModel(worktypename, Constants.TIMESHEET_ENTRY_WORK_TYPE_NAME);
			Common.setDataToModel(customername, Constants.TIMESHEET_ENTRY_CUSTOMER_NAME);
			Common.setDataToModel(projectname, Constants.TIMESHEET_ENTRY_PROJECT_NAME);
			Common.setDataToModel(statuscode, Constants.TIMESHEET_ENTRY_STATUS_CODE);

			var oDialog = new sap.m.BusyDialog();
			oDialog.open();

			jQuery.sap.delayedCall(1000, this, function () {
				oDialog.close();
				return this.NavigateToView("projectTimePage");
			});

		},

		onItemsChange: function (oEvent) {
			var oTable = oEvent.byId("mondayInputList");

			oTable.getItems().forEach(function (oItem) {
				var oContext = oItem.getBindingContext();
				if (oContext && oContext.getObject().status === 'A') {
					oItem.addStyleClass("overdue");
				}
			});
		},
		tapFunc: function (oEvent) {
			var bg = "red";
			var selected_list = oEvent.getId();
			if (selected_list) {
				$("#" + selected_list).css("background-color", bg);
			}
		}
	});

});

/*
			Rnplnr alanı: Network bilgisi(proje) 
			Proje adı: nereden alabiliriz? Eklemek kolay ise buraya ekleyebilir misin? Tek bir database access yapabilmek için.
			Müşteri adı: nereden alabiliriz? Eklemek kolay ise buraya ekleyebilir misin? Tek bir database access yapabilmek için.
			Zzltxa1: aktivite bilgisi  aktivite kodu
			Ltxa1: ise aktivite ile ilgili açıklama. Açıklama boş gözüküyor  tüm aktivitelerin açıklaması boş mudur? Biz kodu mu kullanalım ekranlarda açıklama olarak?
			Awawrt: ise girlen aktivitenin türünü söylüyor. Work type. Awart alanını kastettin diye varsayıyoruz, doğru mudur?
			Atext: alanında da bunun tanımı var.  Work type açıklama Yıllık izin mi hastalık izni mi yoksa çalışma aktivitesi mi bilgisi mevcut.
			
			Zzprofctr: alanı ise aktivite masraf yerine girilmişse doluyor. Örneğin idari personellerin girdiği masraflarda genelde Zzprofctr alanı dolu oluyor. Projeye aktivite girmedikleri için. Biz şu anda timesheet yaptığımız için bu alan dolu ise ilgili satırı bir iş olarak dikkate almayacağız. Doğru mudur?
                   */